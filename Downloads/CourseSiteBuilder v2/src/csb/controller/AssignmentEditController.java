package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Assignment;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.gui.AssignmentItemDialog;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Jay
 */
public class AssignmentEditController {

    AssignmentItemDialog aid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;

    public AssignmentEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        aid = new AssignmentItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    public void handleAddAssignmentItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showAddAssignmentDialog(course.getStartingMonday());

        // user confirm?
        if (aid.wasCompleteSelected()) {
            // GET THE ASSIGNMENT
            Assignment ai = aid.getAssignment();

            // AND ADD IT AS A ROW TO THE TABLE
            course.addAssignment(ai);
            gui.getFileController().markAsEdited(gui);

        } else {
            //cancel so noothing happens
        }
    }

    public void handleEditAssignmentItemRequest(CSB_GUI gui, Assignment itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showEditAssignmentItemDialog(itemToEdit);

        if (aid.wasCompleteSelected()) {
            Assignment ai = aid.getAssignment();
            itemToEdit.setTopics(ai.getTopics());
            itemToEdit.setDate(ai.getDate());
            itemToEdit.setName(ai.getName());
            gui.getFileController().markAsEdited(gui);
        } else {
        }
    }

    public void handleRemoveAssignmentRequest(CSB_GUI gui, Assignment itemToRemove) {
        //PROMPT TO SAVE
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));

        // Gets the selection
        String selection = yesNoCancelDialog.getSelection();

        if (selection.equals(YesNoCancelDialog.YES)) {
            gui.getDataManager().getCourse().removeAssignment(itemToRemove);
            gui.getFileController().markAsEdited(gui);
        }
    }
}
