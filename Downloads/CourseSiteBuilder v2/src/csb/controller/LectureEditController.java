/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Lecture;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.LectureItemDialog;
import csb.gui.YesNoCancelDialog;
import java.util.Collections;
import java.util.List;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Jay
 */
public class LectureEditController {

    LectureItemDialog lid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;

    public LectureEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    //THESE ARE FOR LECTURE ITEMS
    public void handleAddLectureItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showAddLectureDialog();
        if (lid.wasCompleteSelected()) {
            Lecture li = lid.getLecture();

            course.addLecture(li);
            gui.getFileController().markAsEdited(gui);//MARK AS EDITED SO USER CAN SAVE

        } else {
            //CANCEL
        }
    }

    public void handleEditLectureItemRequest(CSB_GUI gui, Lecture lectureToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showEditLectureItemDialog(lectureToEdit);
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // UPDATE THE LECTURE ITEM-JAY
            Lecture li = lid.getLecture();
            lectureToEdit.setTopic(li.getTopic());
            lectureToEdit.setSessions(li.getSessions());
            gui.getFileController().markAsEdited(gui);

        } else {

        }
    }

    public void handleRemoveLectureItemRequest(CSB_GUI gui, Lecture lectureToRemove) {
        //prompt to save=jay
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));

        //get selection
        String selection = yesNoCancelDialog.getSelection();

        // SAID YEA?
        if (selection.equals(YesNoCancelDialog.YES)) {
            gui.getDataManager().getCourse().removeLecture(lectureToRemove);
            gui.getFileController().markAsEdited(gui);

        }
    }

    public void handleMoveUpLectureItemRequest(CSB_GUI gui, Lecture selectedItem) {
        Course course = gui.getDataManager().getCourse();
        List<Lecture> lectureList = course.getLectures();
        int position = lectureList.indexOf(selectedItem);
        if (position != 0) {
            Collections.swap(lectureList, position, position - 1);
        } else {
            Collections.rotate(lectureList, -1);
        }
        gui.getFileController().markAsEdited(gui);

    }

    public void handleMoveDownLectureItemRequest(CSB_GUI gui, Lecture selectedItem) {
        Course course = gui.getDataManager().getCourse();
        List<Lecture> lectureList = course.getLectures();
        int position = lectureList.indexOf(selectedItem);
        if (position != lectureList.size() - 1) {
            Collections.swap(lectureList, position, position + 1);
        } else {
            Collections.rotate(lectureList, 1);
        }
        gui.getFileController().markAsEdited(gui);

    }
}
