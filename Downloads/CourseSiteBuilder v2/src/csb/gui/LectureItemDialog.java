/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.gui;

import csb.data.Course;
import csb.data.Lecture;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import static csb.gui.LectureItemDialog.CANCEL;
import static csb.gui.LectureItemDialog.COMPLETE;
import static csb.gui.LectureItemDialog.EDIT_LECTURE_TITLE;
import static csb.gui.LectureItemDialog.LECTURE_HEADING;
import static csb.gui.LectureItemDialog.SESSIONS_PROMPT;
import static csb.gui.LectureItemDialog.TOPIC_PROMPT;
import static csb.gui.LectureItemDialog.ADD_LECTURE_TITLE;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
/**
 *
 * @author Jay
 */
public class LectureItemDialog extends Stage {
   //This is the Lecture Data Behind this UI
    Lecture lecture;
    
     // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label numberSessionsLabel;
    ComboBox numberSessionsComboBox;
    Label TopicLabel;
    TextField topicTextField;
    Button completeButton;
    Button cancelButton;
    
     // Which Button was Pressed
    String selection;
    
    // CONSTANTS
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String TOPIC_PROMPT = "Topic: ";
    public static final String SESSIONS_PROMPT = "Number of sessions: ";
    public static final String LECTURE_HEADING = "Lecture Details";
    public static final String ADD_LECTURE_TITLE = "Add New Lecture";
    public static final String EDIT_LECTURE_TITLE = "Edit Lecture";
    
    public LectureItemDialog(Stage primaryStage, Course course,  MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // JAY
        headingLabel = new Label(LECTURE_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE TOPIC
        TopicLabel = new Label(TOPIC_PROMPT);
        TopicLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicTextField = new TextField();
        topicTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            lecture.setTopic(newValue);
        });
        
        numberSessionsLabel = new Label(SESSIONS_PROMPT);
        numberSessionsComboBox = new ComboBox();
        numberSessionsComboBox.getItems().addAll(1,2,3);
        numberSessionsComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            lecture.setSessions((int) newValue);
        });
      
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            LectureItemDialog.this.selection = sourceButton.getText();
            LectureItemDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(TopicLabel, 0, 1, 1, 1);
        gridPane.add(topicTextField, 1, 1, 1, 1);
        gridPane.add(numberSessionsLabel, 0, 2 , 1,1);
        gridPane.add(numberSessionsComboBox, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Lecture getLecture() { 
        return lecture;
    }
      
    public Lecture showAddLectureDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_LECTURE_TITLE);
        
        // RESET THE LECTURE ITEM OBJECT WITH DEFAULT VALUES
        lecture = new Lecture();
        
        // LOAD THE UI STUFF
        topicTextField.setText(lecture.getTopic());
        numberSessionsComboBox.setValue(lecture.getSessions());
        
        // AND OPEN IT UP
        this.showAndWait();
        return lecture;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        topicTextField.setText(lecture.getTopic());
        numberSessionsComboBox.setValue(lecture.getSessions());
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditLectureItemDialog(Lecture lectureToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_LECTURE_TITLE);
        
        lecture = new Lecture();
        lecture.setTopic(lectureToEdit.getTopic());
        lecture.setSessions(lectureToEdit.getSessions());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }


  

}
