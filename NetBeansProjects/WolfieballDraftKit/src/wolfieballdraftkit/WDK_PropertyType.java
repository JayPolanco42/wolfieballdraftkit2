/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieballdraftkit;


/**
 *
 * @author Jay
 */
public enum WDK_PropertyType {
    //LOADED FROM properties.xml
//LOADED FROM properties.xml
        PROP_APP_TITLE,
        
        // APPLICATION ICONS
        NEW_DRAFT_ICON,
        LOAD_DRAFT_ICON,
        SAVE_DRAFT_ICON,
        VIEW_SCHEDULE_ICON,
        EXPORT_PAGE_ICON,
        EXIT_ICON,
        ADD_ICON,
        REMOVE_ICON,
        
        
        
        
        MOVE_UP_ICON,
        MOVE_DOWN_ICON,
        PLAYERS_SCREEN_ICON,
        FANTASYTEAMS_SCREEN_ICON,
        FANTASYSTANDINGS_SCREEN_ICON,
        DRAFTS_SCREEN_ICON,
        MLBTEAMS_SCREEN_ICON,
        
        // APPLICATION TOOLTIPS FOR BUTTONS
        NEW_DRAFT_TOOLTIP,
        LOAD_DRAFT_TOOLTIP,
        SAVE_DRAFT_TOOLTIP,
        PLAYER_SCREEN_TOOLTIP,
        FANTASYTEAM_SCREEN_TOOLTIP,
        FANTASYSTANDING_SCREEN_TOOLTIP,
        DRAFT_SCREEN_TOOLTIP,
        MLBTEAMS_SCREEN_TOOLTIP,
        VIEW_SCHEDULE_TOOLTIP,
        EXPORT_PAGE_TOOLTIP,
        DELETE_TOOLTIP,
        EXIT_TOOLTIP,
        ADD_PLAYER_TOOLTIP,
        REMOVE_PLAYER_TOOLTIP,
        
       

        // FOR DRAFT EDIT WORKSPACE
        PLAYER_HEADING_LABEL,
        FANTASYTEAMS_HEADING_LABEL,
        FANTASYSTANDINGS_HEADING_LABEL,
        DRAFT_HEADING_LABEL,
        MLBTEAMS_HEADING_LABEL,
        SEARCH_LABEL
}

