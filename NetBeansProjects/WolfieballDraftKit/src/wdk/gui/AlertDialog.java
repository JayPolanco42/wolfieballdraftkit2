/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static wdk.gui.WDK_GUI.MAIN_STYLE_SHEET;

/**
 *
 * @author Jay
 */
public class AlertDialog extends Stage {

    Scene alertScene;
    VBox alertPane;
    Label alertLabel;
    Button alertButton;

    public AlertDialog(Stage owner, String buttonText) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        alertLabel = new Label();
        alertButton = new Button(buttonText);
        alertButton.setOnAction(e -> {
            this.close();
        });

        alertPane = new VBox(10);
        alertPane.getChildren().addAll(alertLabel, alertButton);
        alertPane.setPadding(new Insets(10, 20, 20, 20));
        alertPane.setAlignment(Pos.CENTER);
        alertPane.setId("toolPane");

        alertScene = new Scene(alertPane);
        alertScene.getStylesheets().add(MAIN_STYLE_SHEET);
        alertLabel.setId("subheading_label");
        this.setScene(alertScene);

    }

    public void show(String message) {
        alertLabel.setText(message);
        this.showAndWait();
    }
}
