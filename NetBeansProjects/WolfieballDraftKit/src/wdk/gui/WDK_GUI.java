/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.file.DraftFileManager;
import wdkcontroller.DraftFileController;
import wdkdata.DraftData;
import wdkdata.Player;
import static wolfieballdraftkit.WDK_Constants.PATH_CSS;
import static wolfieballdraftkit.WDK_Constants.PATH_IMAGES;
import wolfieballdraftkit.WDK_PropertyType;

/**
 *
 * @author Jay
 */
public class WDK_GUI {

    //CONSTONANTS FOR MANAGING STYLE
    static final String MAIN_STYLE_SHEET = PATH_CSS + "wdkStyleSheet.css";
    private DraftData draft;

    Stage primaryStage;
    Scene primaryScene;
    BorderPane wdkPane;

    //THIS BELONGS TO THE TOP TOOLBAR THAT REMAINS ON SCENE
    HBox fileToolbarPane;
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exportDraftButton;
    Button exitButton;

    //This belongs to the Screen Toolbar
    HBox screenToolbarPane;
    Button playerScreenButton;
    Button fantasyTeamsScreenButton;
    Button fantasyStandingsButton;
    Button draftsScreenButton;
    Button mlbTeamsScreenButton;

    //This belongs to Player screen
    VBox playerPane;
    Label playerScreenHeading;

    HBox playerToolsWorkspace;
    Button addPlayerButton;
    Button removePlayerButton;
    Label playerSearchLabel;
    TextField searchPlayerTextField;

    //This belongs to the first HBox in Player Screen
    //These are the radiobuttons on the player screen
    HBox radioButtonPane;
    RadioButton allRadioButton;
    RadioButton cRadioButton;
    RadioButton clRadioButton;
    RadioButton b1RadioButton;
    RadioButton b3RadioButton;
    RadioButton b2RadioButton;
    RadioButton miRadioButton;
    RadioButton ssRadioButton;
    RadioButton ofRadioButton;
    RadioButton uRadioButton;
    RadioButton pRadioButton;

    Scene playerScene;
    //ALL ELEMENTS FOR FANTASY TEAMS SCREEN
    VBox fantasyTeamsPane;
    Label fantasyTeamsScreenHeading;

    //ALL ELEMENTS FOR FANTASY STANDINGS SCREEN
    VBox fantasyStandingsPane;
    Label fantasyStandingsScreenHeading;

    //ALL ELEMENTS FOR DRAFT SCREEN
    VBox draftPane;
    Label draftScreenHeading;

    //ALL ELEMENTS FOR MLB SCREEN
    VBox mlbPane;
    Label mlbScreenHeading;

    TableView<Player> table1;
    TableColumn column12;
    TableColumn column11;
    TableColumn column10;
    TableColumn column9;
    TableColumn column8;
    TableColumn column7;
    TableColumn column6;
    TableColumn column5;
    TableColumn column4;
    TableColumn column3;
    TableColumn column2;
    TableColumn column1;

    ObservableList<Player> filteredlist;
    //Controls all file handling such as New Draft, Load Draft etc..
    DraftFileController draftFileController;

    ObservableList<Player> cList;
    ObservableList<Player> b1List;
    ObservableList<Player> b2List;
    ObservableList<Player> b3List;
    ObservableList<Player> ssList;
    ObservableList<Player> ofList;
    ObservableList<Player> uList;
    ObservableList<Player> pList;
    ObservableList<Player> removalList;
    ObservableList<Player> cReturn;
    ObservableList<Player> clReturn;
    ObservableList<Player> b1Return;
    ObservableList<Player> b2Return;
    ObservableList<Player> b3Return;
    ObservableList<Player> ssReturn;
    ObservableList<Player> ofReturn;
    ObservableList<Player> pReturn;
    ObservableList<Player> hReturn;
    ObservableList<Player> miReturn;
    DraftFileManager draftFileManager;
    //Alert Dialog
    AlertDialog alertDialog;

    public WDK_GUI(Stage primaryStage) {
        this.draft = new DraftData();
        this.primaryStage = primaryStage;
    }

    public Stage getStage() {
        return primaryStage;
    }

    public void initStage() {
        primaryStage.setTitle("Wolfieball Draft Kit");

        Rectangle2D bounds = Screen.getPrimary().getBounds();

        //Sizing the window
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        wdkPane = new BorderPane();
        initToolbar();
        initFirstEventHandlers();
        wdkPane.setTop(fileToolbarPane);
        primaryScene = new Scene(wdkPane);
        primaryScene.getStylesheets().add(MAIN_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        initFileController();

    }

    private void initFileController() {
        initDialogs();
        draftFileController = new DraftFileController(alertDialog, draftFileManager);
    }

    private void initDialogs() {
        alertDialog = new AlertDialog(primaryStage, "Okay");
    }

    private void initScreens() {
        //initializes player screen
        initPlayerScreen();
        //initializes fantasy teams screen
        initFantasyTeamScreen();
        //initializes fantasy standing screen
        initFantasyStandingsScreen();
        //initializes draft screen
        initDraftScreen();
        //initializes mlb teams screen
        initMLBScreen();
    }

    public void initPlayerScreen() {

        playerPane = new VBox(10);
        playerToolsWorkspace = new HBox(10);
        radioButtonPane = new HBox(10);

        playerScreenHeading = initLabel(WDK_PropertyType.PLAYER_HEADING_LABEL, "heading_label");

        addPlayerButton = initChildButton("Add", "editTableButton", WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removePlayerButton = initChildButton("Remove", "editTableButton", WDK_PropertyType.REMOVE_ICON, WDK_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
        playerSearchLabel = initLabel(WDK_PropertyType.SEARCH_LABEL, "textLabel");
        searchPlayerTextField = initTextField("textField", "Search for a Player", 10, true);
        playerToolsWorkspace.getChildren().addAll(addPlayerButton, removePlayerButton, playerSearchLabel, searchPlayerTextField);

        playerToolsWorkspace.setAlignment(Pos.CENTER);
        HBox.setHgrow(searchPlayerTextField, Priority.ALWAYS);//Expands textfield to take remaining space
        playerPane.setId("toolPane");

        allRadioButton = initRadioButton("All", "radioButton");
        cRadioButton = initRadioButton("C", "radioButton");
        clRadioButton = initRadioButton("CL", "radioButton");
        b1RadioButton = initRadioButton("1B", "radioButton");
        b3RadioButton = initRadioButton("3B", "radioButton");
        b2RadioButton = initRadioButton("2B", "radioButton");
        miRadioButton = initRadioButton("MI", "radioButton");
        ssRadioButton = initRadioButton("SS", "radioButton");
        ofRadioButton = initRadioButton("OF", "radioButton");
        uRadioButton = initRadioButton("U", "radioButton");
        pRadioButton = initRadioButton("P", "radioButton");
        radioButtonPane.getChildren().addAll(allRadioButton, cRadioButton, clRadioButton, b1RadioButton,
                b3RadioButton, b2RadioButton, miRadioButton, ssRadioButton, ofRadioButton, uRadioButton, pRadioButton);
        radioButtonPane.setId("blueBar");

        table1 = new TableView();
        table1.setStyle("tableStyle");
        column1 = new TableColumn("First");
        column2 = new TableColumn("Last");
        column3 = new TableColumn("Pro Team");
        column4 = new TableColumn("Year of Birth");
        column5 = new TableColumn("Positions");
        column6 = new TableColumn("R/W");
        column7 = new TableColumn("HR/SV");
        column8 = new TableColumn("RBI/K");
        column9 = new TableColumn("SB/ERA");
        column10 = new TableColumn("BA/WHIP");
        column11 = new TableColumn("Estimated Value");
        column12 = new TableColumn("Notes");

//        Player player = new Player();//TODO REMOVE
//        draft.getPlayers().add(player);
        column1.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        column2.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        column3.setCellValueFactory(new PropertyValueFactory<>("proTeam"));
        column4.setCellValueFactory(new PropertyValueFactory<>("birthYear"));
        column5.setCellValueFactory(new PropertyValueFactory<>("positionsList"));
        column6.setCellValueFactory(new PropertyValueFactory<>("RW"));
        column7.setCellValueFactory(new PropertyValueFactory<>("hrSV"));
        column8.setCellValueFactory(new PropertyValueFactory<>("rbiK"));
        column9.setCellValueFactory(new PropertyValueFactory<>("sbEra"));
        column10.setCellValueFactory(new PropertyValueFactory<>("baWhip"));
        column11.setCellValueFactory(new PropertyValueFactory<>("salary"));
        column12.setCellValueFactory(new PropertyValueFactory<>("notes"));
        column12.setCellFactory(TextFieldTableCell.forTableColumn());
        filteredlist = FXCollections.observableArrayList();
        table1.setEditable(true);
        table1.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table1.getColumns().addAll(column1, column2, column3, column4, column5, column6, column7, column8, column9, column10, column11, column12);
        playerPane.getChildren().addAll(playerScreenHeading, playerToolsWorkspace, radioButtonPane, table1);//ADDING TOP LABEL

    }

    public void initWorkSpace() {
        initScreens();
        showScreen(playerPane);
        initScreenbar();
        initOtherEventHandlers();
    }

    public void initFantasyTeamScreen() {
        fantasyTeamsPane = new VBox(10);
        fantasyTeamsPane.setId("toolPane");
        fantasyTeamsScreenHeading = initLabel(WDK_PropertyType.FANTASYTEAMS_HEADING_LABEL, "heading_label");

        fantasyTeamsPane.getChildren().add(fantasyTeamsScreenHeading);

    }

    public void initFantasyStandingsScreen() {
        fantasyStandingsPane = new VBox(10);
        fantasyStandingsPane.setId("toolPane");

        fantasyStandingsScreenHeading = initLabel(WDK_PropertyType.FANTASYSTANDINGS_HEADING_LABEL, "heading_label");

        fantasyStandingsPane.getChildren().add(fantasyStandingsScreenHeading);
    }

    public void initDraftScreen() {
        draftPane = new VBox(10);
        draftPane.setId("toolPane");

        draftScreenHeading = initLabel(WDK_PropertyType.DRAFT_HEADING_LABEL, "heading_label");

        draftPane.getChildren().add(draftScreenHeading);
    }

    public void initMLBScreen() {
        mlbPane = new VBox(10);
        mlbPane.setId("toolPane");

        mlbScreenHeading = initLabel(WDK_PropertyType.MLBTEAMS_HEADING_LABEL, "heading_label");

        mlbPane.getChildren().add(mlbScreenHeading);
    }

    public void initFirstEventHandlers() {
        newDraftButton.setOnAction(e -> {
            initWorkSpace();
            draftFileController.doNewDraftRequest(this);
        });
    }

    public void initOtherEventHandlers() {
        playerScreenButton.setOnAction(e -> {
            showScreen(playerPane);
        });
        fantasyTeamsScreenButton.setOnAction(e -> {
            showScreen(fantasyTeamsPane);
        });
        fantasyStandingsButton.setOnAction(e -> {
            showScreen(fantasyStandingsPane);
        });
        draftsScreenButton.setOnAction(e -> {
            showScreen(draftPane);
        });
        mlbTeamsScreenButton.setOnAction(e -> {
            showScreen(mlbPane);
        });
        allRadioButton.setOnAction(e -> {
            seeAll();
            columnsAll();

        });
        cRadioButton.setOnAction(e -> {
            if (cRadioButton.isSelected()) {
                cReturn = filterPosition(cList);
                columnsHitters();
            } else {
                putBackPlayers(cReturn);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        clRadioButton.setOnAction(e -> {
            if (clRadioButton.isSelected()) {
                clReturn = filterDualPosition(b1List, b3List);
                columnsHitters();
            } else {
                putBackPlayers(clReturn);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        b1RadioButton.setOnAction(e -> {
            if (b1RadioButton.isSelected()) {
                b1Return = filterPosition(b1List);
                columnsHitters();

            } else {
                putBackPlayers(b1Return);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        b3RadioButton.setOnAction(e -> {
            if (b3RadioButton.isSelected()) {
                b3Return = filterPosition(b3List);
                columnsHitters();
            } else {
                putBackPlayers(b3Return);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        b2RadioButton.setOnAction(e -> {
            if (b2RadioButton.isSelected()) {
                b2Return = filterPosition(b2List);
                columnsHitters();
            } else {
                putBackPlayers(b2Return);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        miRadioButton.setOnAction(e -> {
            if (miRadioButton.isSelected()) {
                miReturn = filterDualPosition(b2List, ssList);
                columnsHitters();
            } else {
                putBackPlayers(miReturn);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        ssRadioButton.setOnAction(e -> {
            if (ssRadioButton.isSelected()) {
                ssReturn = filterPosition(ssList);
                columnsHitters();
            } else {
                putBackPlayers(ssReturn);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        ofRadioButton.setOnAction(e -> {
            if (ofRadioButton.isSelected()) {
                ofReturn = filterPosition(ofList);
                columnsHitters();

            } else {
                putBackPlayers(ofReturn);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        uRadioButton.setOnAction(e -> {
            if (uRadioButton.isSelected()) {
                hReturn = filterPitchers(pList);
                columnsHitters();
            } else {
                putBackPlayers(hReturn);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        pRadioButton.setOnAction(e -> {
            if (pRadioButton.isSelected()) {
                pReturn = filterPosition(pList);
                columnsPitchers();
            } else {
                putBackPlayers(pReturn);
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });

        searchPlayerTextField.setOnKeyReleased(e -> {
            String searchString = searchPlayerTextField.getText().toLowerCase();
            filterList(searchString);
        });
        column12.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Player, String>>() {

                    @Override
                    public void handle(TableColumn.CellEditEvent<Player, String> t) {
                        ((Player) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())).setNotes(t.getNewValue());
                    }
                });

    }

    public boolean isHitterSelected() {
        if (cRadioButton.isSelected()
                || clRadioButton.isSelected()
                || b1RadioButton.isSelected()
                || b3RadioButton.isSelected()
                || b2RadioButton.isSelected()
                || miRadioButton.isSelected()
                || ssRadioButton.isSelected()
                || ofRadioButton.isSelected()
                || uRadioButton.isSelected()) {
            return true;
        } else {
            return false;
        }
    }

    public void columnsAll() {
        column6.setText("R/W");
        column7.setText("HR/SV");
        column8.setText("RBI/ERA");
        column9.setText("SB/ERA");
        column10.setText("BA/WHIP");

    }

    public void columnsHitters() {
        column6.setText("R");
        column7.setText("HR");
        column8.setText("RBI");
        column9.setText("SB");
        column10.setText("BA");
    }

    public void columnsPitchers() {
        column6.setText("W");
        column7.setText("SV");
        column8.setText("K");
        column9.setText("ERA");
        column10.setText("WHIP");
    }

    public void seeAll() {
        cRadioButton.setSelected(false);
        b1RadioButton.setSelected(false);
        clRadioButton.setSelected(false);
        b3RadioButton.setSelected(false);
        b2RadioButton.setSelected(false);
        miRadioButton.setSelected(false);
        ssRadioButton.setSelected(false);
        ofRadioButton.setSelected(false);
        uRadioButton.setSelected(false);
        pRadioButton.setSelected(false);
        setTable();
    }

    public ObservableList<Player> filterList(String searchString) {
        filteredlist.clear();
        for (int i = 0; i < draft.getPlayers().size(); i++) {
            String firstName = draft.getPlayers().get(i).getFirstName().toLowerCase();
            String lastName = draft.getPlayers().get(i).getLastName().toLowerCase();
            if (firstName.startsWith(searchString) || lastName.startsWith(searchString)) {
                filteredlist.add(draft.getPlayers().get(i));
            }

        }
        table1.setItems(filteredlist);
        return filteredlist;
    }

    public ObservableList<Player> filterPitchers(ObservableList<Player> list) {
        removalList = FXCollections.observableArrayList();
        for (int i = 0; i < filteredlist.size(); i++) {
            Player p = filteredlist.get(i);
            if (list.contains(p)) {
                removalList.add(p);
            }
        }
        filteredlist.removeAll(removalList);
        table1.setItems(filteredlist);
        return removalList;
    }

    public ObservableList<Player> filterDualPosition(ObservableList<Player> list, ObservableList<Player> list2) {
        removalList = FXCollections.observableArrayList();
        for (int i = 0; i < filteredlist.size(); i++) {
            Player p = filteredlist.get(i);
            if ((!list.contains(p)) && (!list2.contains(p))) {
                removalList.add(p);
            }
        }
        filteredlist.removeAll(removalList);
        table1.setItems(filteredlist);
        return removalList;
    }

    public ObservableList<Player> filterPosition(ObservableList<Player> list) {
        removalList = FXCollections.observableArrayList();
        for (int i = 0; i < filteredlist.size(); i++) {
            Player p = filteredlist.get(i);
            if (!list.contains(p)) {
                removalList.add(p);
            }
        }
        filteredlist.removeAll(removalList);
        table1.setItems(filteredlist);
        return removalList;
    }

    public void putBackPlayers(ObservableList<Player> list) {
        filteredlist.addAll(list);
        table1.setItems(filteredlist);
        removalList.clear();
    }

    public void setPosCategories() {
        cList = FXCollections.observableArrayList();
        b1List = FXCollections.observableArrayList();
        b2List = FXCollections.observableArrayList();
        b3List = FXCollections.observableArrayList();
        ssList = FXCollections.observableArrayList();
        ofList = FXCollections.observableArrayList();
        pList = FXCollections.observableArrayList();
        for (int i = 0; i < draft.getPlayers().size(); i++) {
            Player p = draft.getPlayers().get(i);
            ObservableList<String> positionsList = p.getPositionsList();
            for (int j = 0; j < positionsList.size(); j++) {
                String pos = positionsList.get(j);
                if (pos.equals("C")) {
                    cList.add(p);
                } else if (pos.equals("1B")) {
                    b1List.add(p);
                } else if (pos.equals("2B")) {
                    b2List.add(p);
                } else if (pos.equals("3B")) {
                    b3List.add(p);

                } else if (pos.equals("SS")) {
                    ssList.add(p);

                } else if (pos.equals("OF")) {
                    ofList.add(p);
                } else if (pos.equals("P")) {
                    pList.add(p);
                }
            }

        }
        cReturn = FXCollections.observableArrayList();
        b1Return = FXCollections.observableArrayList();
        b2Return = FXCollections.observableArrayList();
        b3Return = FXCollections.observableArrayList();
        ssReturn = FXCollections.observableArrayList();
        ofReturn = FXCollections.observableArrayList();
        pReturn = FXCollections.observableArrayList();
        hReturn = FXCollections.observableArrayList();
        clReturn = FXCollections.observableArrayList();
        miReturn = FXCollections.observableArrayList();
    }

    public void loadTables(DraftData draft) {
        this.setDraft(draft);
        setPosCategories();
        setTable();
        //add all tables that need to be reloaded
    }

    public void setTable() {
        filteredlist.addAll(draft.getPlayers());
        table1.setItems(filteredlist);
    }

    //Constructs TOP Toolbar
    private void initToolbar() {
        fileToolbarPane = new HBox();

        newDraftButton = initChildButton("New", "toolbarButton", WDK_PropertyType.NEW_DRAFT_ICON, WDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        loadDraftButton = initChildButton("Load", "toolbarButton", WDK_PropertyType.LOAD_DRAFT_ICON, WDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        saveDraftButton = initChildButton("Save", "toolbarButton", WDK_PropertyType.SAVE_DRAFT_ICON, WDK_PropertyType.SAVE_DRAFT_TOOLTIP, true);
        exportDraftButton = initChildButton("Export", "toolbarButton", WDK_PropertyType.EXPORT_PAGE_ICON, WDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton("Exit", "toolbarButton", WDK_PropertyType.EXIT_ICON, WDK_PropertyType.EXIT_TOOLTIP, false);
        fileToolbarPane.getChildren().addAll(newDraftButton, loadDraftButton, saveDraftButton, exportDraftButton, exitButton);
        fileToolbarPane.setPadding(new Insets(20, 20, 20, 20));
        fileToolbarPane.setSpacing(18);
        fileToolbarPane.setId("blueBar");
        fileToolbarPane.setAlignment(Pos.BASELINE_LEFT);

    }

    //Constructs Screen Buttons
    private void initScreenbar() {
        screenToolbarPane = new HBox();

        playerScreenButton = initChildButton("Players", "toolbarButton", WDK_PropertyType.PLAYERS_SCREEN_ICON, WDK_PropertyType.PLAYER_SCREEN_TOOLTIP, false);
        fantasyTeamsScreenButton = initChildButton("Fantasy Teams", "toolbarButton", WDK_PropertyType.FANTASYTEAMS_SCREEN_ICON, WDK_PropertyType.FANTASYTEAM_SCREEN_TOOLTIP, false);
        fantasyStandingsButton = initChildButton("Fantasy Standings", "toolbarButton", WDK_PropertyType.FANTASYSTANDINGS_SCREEN_ICON, WDK_PropertyType.FANTASYSTANDING_SCREEN_TOOLTIP, false);
        draftsScreenButton = initChildButton("Drafts", "toolbarButton", WDK_PropertyType.DRAFTS_SCREEN_ICON, WDK_PropertyType.DRAFT_SCREEN_TOOLTIP, false);
        mlbTeamsScreenButton = initChildButton("MLB Teams", "toolbarButton", WDK_PropertyType.MLBTEAMS_SCREEN_ICON, WDK_PropertyType.MLBTEAMS_SCREEN_TOOLTIP, false);
        screenToolbarPane.getChildren().addAll(playerScreenButton, fantasyTeamsScreenButton, fantasyStandingsButton, draftsScreenButton, mlbTeamsScreenButton);

        screenToolbarPane.setPadding(new Insets(20, 20, 20, 20));
        screenToolbarPane.setSpacing(18);
        screenToolbarPane.setId("blueBar");
        screenToolbarPane.setAlignment(Pos.BASELINE_LEFT);

        wdkPane.setBottom(screenToolbarPane);
    }

    private Button initChildButton(String buttonText, String styleID, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        ImageView imageView = new ImageView(buttonImage);
        button.setId(styleID);
        button.setGraphic(imageView);
        button.setText(buttonText);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        return button;
    }

    public TextField initTextField(String styleID, String initialText, int size, boolean editable) {
        TextField textField = new TextField();
        textField.setPrefColumnCount(size);
        textField.setText(initialText);
        textField.setId(styleID);
        textField.setEditable(editable);
        return textField;
    }

    public void showScreen(Pane pane) {
        wdkPane.setCenter(pane);
    }

    public void initGUI() {
        initStage(); //Initializes the stage
    }

    public Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label((labelText));
        label.getStyleClass().add(styleClass);
        return label;
    }

    public RadioButton initRadioButton(String radioButtonLabel, String styleID) {
        RadioButton radioButton = new RadioButton(radioButtonLabel);
        radioButton.setId(styleID);
        radioButton.setPadding(new Insets(20, 20, 20, 20));
        return radioButton;
    }

    /**
     * @return the draftFileManager
     */
    public DraftFileManager getDraftFileManager() {
        return draftFileManager;
    }

    /**
     * @param draftFileManager the draftFileManager to set
     */
    public void setDraftFileManager(DraftFileManager draftFileManager) {
        this.draftFileManager = draftFileManager;
    }

    /**
     * @return the draft
     */
    public DraftData getDraft() {
        return draft;
    }

    /**
     * @param draft the draft to set
     */
    public void setDraft(DraftData draft) {
        this.draft = draft;
    }

}
