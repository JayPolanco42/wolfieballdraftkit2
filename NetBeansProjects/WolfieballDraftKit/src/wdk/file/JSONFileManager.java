/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import javafx.collections.ObservableList;
import wdkdata.DraftData;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonValue;
import wdkdata.Player;

/**
 *
 * @author Jay
 */
public class JSONFileManager implements DraftFileManager {

    String JSON_PITCHERS = "Pitchers";
    String JSON_HITTERS = "Hitters";
    
    //INFORMATION NEEDED FROM HITTERS
    String JSON_TEAM = "TEAM";
    String JSON_LAST_NAME = "LAST_NAME";
    String JSON_FIRST_NAME = "FIRST_NAME";
    String JSON_QP = "QP";
    String JSON_AB = "AB";
    String JSON_R = "R";
    String JSON_H = "H";
    String JSON_HR = "HR";
    String JSON_RBI = "RBI";
    String JSON_SB = "SB";
    String JSON_NOTES = "NOTES";
    String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH";
    
    //INFORMATION NEEDED FROM PITCHERS
    String JSON_IP = "IP";
    String JSON_ER = "ER";
    String JSON_W = "W";
    String JSON_SV= "SV";
    String JSON_BB = "BB";
    String JSON_K = "K";
    
    String JSON_EXT = ".json";
    String SLASH = "/";
    
    private String team;
    private String lastName; 
    private String firstName;
    private String qp;
    private int ab;
    

    @Override
    public void loadPlayers(DraftData draftToLoad, String hitterPath, String pitcherPath) throws IOException {
        JsonObject jsonHitters = loadJSONFile(hitterPath);

        ObservableList<Player> players = draftToLoad.getPlayers();
        JsonArray jsonHitterArray = jsonHitters.getJsonArray(JSON_HITTERS);
        for (int i = 0; i < jsonHitterArray.size(); i++) {
            JsonObject currentHitter = jsonHitterArray.getJsonObject(i);
            String team = currentHitter.getString(JSON_TEAM);
            String lastName = currentHitter.getString(JSON_LAST_NAME);
            String firstName = currentHitter.getString(JSON_FIRST_NAME);
            String qp = currentHitter.getString(JSON_QP);
            int ab = Integer.parseInt(currentHitter.getString(JSON_AB));
            int r = Integer.parseInt(currentHitter.getString(JSON_R));
            int h= Integer.parseInt(currentHitter.getString(JSON_H));
            int hr = Integer.parseInt(currentHitter.getString(JSON_HR));
            double ba = (double)h/(double)ab;
            int rbi = Integer.parseInt(currentHitter.getString(JSON_RBI));
            int sb = Integer.parseInt(currentHitter.getString(JSON_SB));
            String notes = currentHitter.getString(JSON_NOTES);
            int yearOfBirth = Integer.parseInt(currentHitter.getString(JSON_YEAR_OF_BIRTH));
            String nationOfBirth = currentHitter.getString(JSON_NATION_OF_BIRTH);
            Player currentPlayer = new Player(firstName, lastName, team, yearOfBirth, r, rbi, hr, sb, ba, 0,  notes, false);
            //edit for multiple qualifying positions
            currentPlayer.setPositionsList(Arrays.asList(qp.split("_")));
            players.add(currentPlayer);
        }
        
         JsonObject jsonPitchers = loadJSONFile(pitcherPath);
         JsonArray jsonPitcherArray = jsonPitchers.getJsonArray(JSON_PITCHERS);
            for (int i = 0; i < jsonPitcherArray.size(); i++) {
            JsonObject currentPitcher = jsonPitcherArray.getJsonObject(i);
            String team = currentPitcher.getString(JSON_TEAM);
            String lastName = currentPitcher.getString(JSON_LAST_NAME);
            String firstName = currentPitcher.getString(JSON_FIRST_NAME);
            double ip = Double.parseDouble(currentPitcher.getString(JSON_IP));
            int er = Integer.parseInt(currentPitcher.getString(JSON_ER));
            int w= Integer.parseInt(currentPitcher.getString(JSON_W));
            int sv = Integer.parseInt(currentPitcher.getString(JSON_SV));
            int h = Integer.parseInt(currentPitcher.getString(JSON_H));
            int bb = Integer.parseInt(currentPitcher.getString(JSON_BB));
            int k = Integer.parseInt(currentPitcher.getString(JSON_K));
            double era = (double) er*9/ (double) ip;
            double whip = ((double)w+(double)h)/ip;
            String notes = currentPitcher.getString(JSON_NOTES);
            int yearOfBirth = Integer.parseInt(currentPitcher.getString(JSON_YEAR_OF_BIRTH));
            String nationOfBirth = currentPitcher.getString(JSON_NATION_OF_BIRTH);
            Player currentPlayer = new Player(firstName, lastName, team, yearOfBirth, w, k, sv, era, whip, 0,  notes, true);
            //edit for multiple qualifying positions
            currentPlayer.getPositionsList().add("P");
            players.add(currentPlayer);
        }


    }

    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
}
