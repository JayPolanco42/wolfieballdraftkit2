/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.IOException;
import wdkdata.DraftData;

/**
 *
 * @author Jay
 */
public interface DraftFileManager {
        public void loadPlayers(DraftData draftToLoad, String hitterPath, String pitcherPath) throws IOException;

}
