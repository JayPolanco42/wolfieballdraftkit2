/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdkdata;

/**
 *
 * @author Jay
 */
public class DraftManager {
    //This is the draft data being edited
    private DraftData draft;
    
    DraftViewData draftView;
    
    public DraftManager(){
        draft = new DraftData();
    }

    /**
     * @return the draft
     */
    public DraftData getDraft() {
        return draft;
    }

    /**
     * @param draft the draft to set
     */
    public void setDraft(DraftData draft) {
        this.draft = draft;
    }
    
    public void resetDraft(){
        draft.getPlayers().clear();
    }
    
}
