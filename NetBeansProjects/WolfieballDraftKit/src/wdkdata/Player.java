/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdkdata;

import static java.lang.Double.NaN;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Jay
 */
public class Player {

    private StringProperty firstName;
    private StringProperty lastName;
    private StringProperty proTeam;
    private IntegerProperty birthYear;
    private ObservableList<String> positionsList;
    private IntegerProperty RW;
    private IntegerProperty hrSV;
    private IntegerProperty rbiK;
    private DoubleProperty sbEra;
    private DoubleProperty baWhip;
    private IntegerProperty salary;
    private StringProperty notes;
    private boolean isPitcher;

    private static String DEFAULT_NAME = "<ENTER NAME>";
    private static String DEFAULT_TEAM = "<ENTER TEAM>";
    private static int DEFAULT_SALARY = 0;

    public Player(String firstName, String lastName, String proTeam, int birthYear, int RW, int rbiK, int hrSV, double sbEra, double baWhip,
    int salary, String notes, boolean isPitcher
    ) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.proTeam = new SimpleStringProperty(proTeam);
        this.birthYear = new SimpleIntegerProperty(birthYear);
        positionsList = FXCollections.observableArrayList();
        this.RW = new SimpleIntegerProperty(RW);
        this.rbiK = new SimpleIntegerProperty(rbiK);
        this.hrSV = new SimpleIntegerProperty(hrSV);
        this.sbEra = new SimpleDoubleProperty(sbEra);
        this.baWhip = new SimpleDoubleProperty(baWhip);
        this.salary = new SimpleIntegerProperty(salary);
        this.notes = new SimpleStringProperty(notes);
        this.isPitcher = isPitcher;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName.get();
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(StringProperty firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName.get();
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(StringProperty lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the proTeam
     */
    public String getProTeam() {
        return proTeam.get();
    }

    /**
     * @param proTeam the proTeam to set
     */
    public void setProTeam(StringProperty proTeam) {
        this.proTeam = proTeam;
    }

    /**
     * @return the birthYear
     */
    public int getBirthYear() {
        return birthYear.get();
    }

    /**
     * @param birthYear the birthYear to set
     */
    public void setBirthYear(IntegerProperty birthYear) {
        this.birthYear = birthYear;
    }

    /**
     * @return the rW
     */
    public int getRW() {
        return RW.get();
    }

    /**
     * @param rW the rW to set
     */
    public void setRW(IntegerProperty RW) {
        this.RW = RW;
    }

    /**
     * @return the hrSV
     */
    public int getHrSV() {
        return hrSV.get();
    }

    /**
     * @param hrSV the hrSV to set
     */
    public void setHrSV(IntegerProperty hrSV) {
        this.hrSV = hrSV;
    }

    /**
     * @return the salary
     */
    public int getSalary() {
        return salary.get();
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(IntegerProperty salary) {
        this.salary = salary;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes.get();
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = new SimpleStringProperty(notes);
    }

    /**
     * @return the positionsList
     */
    public ObservableList<String> getPositionsList() {
        return positionsList;
    }

    /**
     * @param positionsList the positionsList to set
     */
    public void setPositionsList(List<String> positionsList) {
        this.positionsList.addAll(positionsList);
    }
    public void addToPosList(String qualPos){
        positionsList.add(qualPos);
    }

    /**
     * @return the rbiK
     */
    public int getRbiK() {
        return rbiK.get();
    }

    /**
     * @param rbiK the rbiK to set
     */
    public void setRbiK(IntegerProperty rbiK) {
        this.rbiK = rbiK;
    }

    /**
     * @return the sbEra
     */
    public Number getSbEra() {
        if (isPitcher) {
            return (int) sbEra.get();
        }
        return sbEra.get();
    }

    /**
     * @param sbEra the sbEra to set
     */
    public void setSbEra(DoubleProperty sbEra) {
        this.sbEra = sbEra;
    }

    /**
     * @return the baWhip
     */
    public double getBaWhip() {
        if(Double.isNaN(baWhip.get())){
            return 0.0;
        }
        return baWhip.get();
    }

    /**
     * @param baWhip the baWhip to set
     */
    public void setBaWhip(DoubleProperty baWhip) {
        this.baWhip = baWhip;
    }

    /**
     * @return the isPitcher
     */
    public boolean isIsPitcher() {
        return isPitcher;
    }

    /**
     * @param isPitcher the isPitcher to set
     */
    public void setIsPitcher(boolean isPitcher) {
        this.isPitcher = isPitcher;
    }

}
