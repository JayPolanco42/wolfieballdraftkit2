/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdkdata;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Jay
 */
    
public class DraftData {
    // INIT THE DRAFT STUFF
    private ObservableList<Player> players;
    
    
    public DraftData() {
        players = FXCollections.observableArrayList(); 
    }

    /**
     * @return the fantasyTeams
     */
    public ObservableList<Player> getPlayers() {
        return players;
    }

    /**
     * @param fantasyTeams the fantasyTeams to set
     */
    public void setPlayers(ObservableList<Player> players) {
        this.players = players;
    }
    
    public void clearPlayers(){
        players.clear();
    }
}