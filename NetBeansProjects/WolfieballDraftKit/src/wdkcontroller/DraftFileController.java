/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdkcontroller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import wdk.file.DraftFileManager;
import wdk.gui.AlertDialog;
import wdk.gui.WDK_GUI;
import wdkdata.DraftData;
import wdkdata.DraftManager;
import wdkdata.Player;
import static wolfieballdraftkit.WDK_Constants.JSON_FILE_PATH_HITTERS;
import static wolfieballdraftkit.WDK_Constants.JSON_FILE_PATH_PITCHERS;

/**
 *
 * @author Jay
 */
public class DraftFileController {

    private boolean saved;
    DraftFileManager manager;
    WDK_GUI gui;
    AlertDialog alertDialog;

    public DraftFileController(AlertDialog alertDialog, DraftFileManager manager) {
        this.alertDialog = alertDialog;
        this.manager = manager;
        saved = true;
    }

    public void doNewDraftRequest(WDK_GUI gui) {
        boolean continueToMakeNew = true;
        if (continueToMakeNew) {
            alertDialog.show("New Draft Created but not yet Saved");
            DraftManager draftManager = new DraftManager();
            DraftData currentDraft = draftManager.getDraft();
            try {
                manager.loadPlayers(currentDraft, JSON_FILE_PATH_HITTERS, JSON_FILE_PATH_PITCHERS);
                gui.loadTables(currentDraft);

            } catch (IOException ex) {
                Logger.getLogger(DraftFileController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
    

}
