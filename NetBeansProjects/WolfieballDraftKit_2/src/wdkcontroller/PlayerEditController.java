/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdkcontroller;

import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.gui.AlertDialog;
import wdk.gui.PlayerDialog;
import wdk.gui.WDK_GUI;
import wdk.gui.YesNoCancelDialog;
import wdkdata.Contract;
import wdkdata.DraftData;
import wdkdata.DraftManager;
import wdkdata.Player;
import static wolfieballdraftkit.WDK_PropertyType.REMOVE_PLAYER_MESSAGE;

/**
 *
 * @author Jay
 */
public class PlayerEditController {

    PlayerDialog playerDialog;
    AlertDialog alertDialog;
    YesNoCancelDialog yesnoDialog;

    public PlayerEditController(Stage initPrimaryStage, DraftData draft, AlertDialog alertDialog, YesNoCancelDialog yesnoCancelDialog) {
        playerDialog = new PlayerDialog(initPrimaryStage, draft, alertDialog);
        this.alertDialog = alertDialog;
        this.yesnoDialog = yesnoCancelDialog;
    }

    public void handleAddPlayer(WDK_GUI gui) {
        DraftManager ddm = gui.getDraftManager();
        DraftData draft = ddm.getDraft();
        Player player = playerDialog.showAddPlayer();
        if (playerDialog.wasCompletedSelected()) {
            draft.getFantasyTeams().get(0).getPlayerList().add(player);
//            gui.getFileController().markAsEdited(gui);//MARK AS EDITED SO USER CAN SAVE

        } else {
            //CANCEL
        }
    }

    public void handleRemovePlayerRequest(WDK_GUI gui, Player player) {
        yesnoDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_PLAYER_MESSAGE));

        String selection = yesnoDialog.getSelection();

        if (selection.equals(YesNoCancelDialog.YES)) {
            gui.getDraftManager().getDraft().getFantasyTeams().get(0).getPlayerList().remove(player);
//            gui.getFileController().markAsEdited(gui);

        } else {

        }
    }

    public void handleEditPlayerRequest(WDK_GUI gui, Player player) {
        DraftManager ddm = gui.getDraftManager();
        DraftData draft = ddm.getDraft();

        Player player2 = playerDialog.showEditPlayer(player);

        if (playerDialog.wasCompletedSelected()) {
            player2.getRefToCurrentTeam().getPlayerList().add(player2);
            for (int i = 0; i < draft.getFantasyTeams().size(); i++) {
                if (draft.getFantasyTeams().get(i).getPlayerList().contains(player)) {
                    draft.getFantasyTeams().get(i).getPlayerList().remove(player);
                }
            }
            if (draft.getFreeAgents().getPlayerList().contains(player)) {
                draft.getFreeAgents().getPlayerList().remove(player);
            }
            System.out.println("Got here once");
            if (player2.getRefToCurrentTeam() != null) {
                player2.getRefToCurrentTeam().countTeam();
                player2.getRefToCurrentTeam().updateTeamStats();
            }
            if (player.getRefToCurrentTeam() != null) {
                player.getRefToCurrentTeam().countTeam();
                player.getRefToCurrentTeam().updateTeamStats();
            }
//            gui.getFileController().markAsEdited(gui);//MARK AS EDITED SO USER CAN SAVE
        }
        if(playerDialog.getSelection().equals("false")){
            player2.setSalary(0);
            player2.setRefToCurrentTeam(draft.getFreeAgents());
            player2.setContract(Contract.X);
            player2.setCurrentPos(player2.getPositionsList().get(0));
            player.getRefToCurrentTeam().getPlayerList().remove(player);
            draft.getFreeAgents().getPlayerList().add(player2);
            player2.getRefToCurrentTeam().updateTeamStats();
        }
        if(playerDialog.getSelection().equals("taxi")){
            player.getRefToCurrentTeam().getPlayerList().remove(player);
            player2.getRefToCurrentTeam().getPlayerList().add(player2);
        }
        if (!playerDialog.wasCompletedSelected()) {
            playerDialog.hide();
        }
                    gui.getDraftManager().getDraft().updateEstimatedValues();

    }
    
}
