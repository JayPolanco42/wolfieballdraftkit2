/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdkcontroller;

import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.gui.AlertDialog;
import wdk.gui.PlayerDialog;
import wdk.gui.TeamDialog;
import wdk.gui.WDK_GUI;
import wdk.gui.YesNoCancelDialog;
import wdkdata.DraftData;
import wdkdata.Team;
import static wolfieballdraftkit.WDK_PropertyType.REMOVE_PLAYER_MESSAGE;
import static wolfieballdraftkit.WDK_PropertyType.REMOVE_TEAM_MESSAGE;

/**
 *
 * @author Jay
 */
public class TeamEditController {

    TeamDialog teamDialog;
    AlertDialog alertDialog;
    YesNoCancelDialog yesnoDialog;

    public TeamEditController(Stage initPrimaryStage, DraftData draft, AlertDialog alertDialog, YesNoCancelDialog yesnoCancelDialog) {
        teamDialog = new TeamDialog(initPrimaryStage, draft, alertDialog);
        this.alertDialog = alertDialog;
        this.yesnoDialog = yesnoCancelDialog;
    }

    public void handleAddTeamRequest(WDK_GUI gui) {
        ObservableList<Team> fantasyTeams = gui.getDraftManager().getDraft().getFantasyTeams();
        Team team = teamDialog.showAddTeamDialog();
        if (teamDialog.wasCompletedSelected()) {
            gui.getDraftManager().getDraft().addTeam(team);
            gui.getDraftManager().getDraft().updateEstimatedValues();

        } else {
            teamDialog.hide();
        }
    }

    public void handleRemoveTeamRequest(WDK_GUI gui, Team team) {
        yesnoDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_TEAM_MESSAGE));

        String selection = yesnoDialog.getSelection();

        if (selection.equals(YesNoCancelDialog.YES)) {
            gui.getDraftManager().getDraft().getFreeAgents().getPlayerList().addAll(team.getPlayerList());
            gui.getDraftManager().getDraft().getFantasyTeams().remove(team);//TODO 
            gui.getDraftManager().getDraft().updateEstimatedValues();
//            gui.getFileController().markAsEdited(gui);
        }

    }

    public void handleEditTeamRequest(WDK_GUI gui, Team origTeam) {
        Team team = teamDialog.showEditTeamDialog(origTeam);
        if (teamDialog.wasCompletedSelected()) {
            origTeam.setOwnerName(team.getOwnerName());
            origTeam.setTeamName(team.getTeamName());
            //mark edited TODO
        }

    }

}
