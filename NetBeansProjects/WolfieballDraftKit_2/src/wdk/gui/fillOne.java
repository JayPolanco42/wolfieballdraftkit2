/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import wdkdata.Contract;
import wdkdata.DraftData;
import wdkdata.DraftManager;
import wdkdata.Player;
import wdkdata.Team;

/**
 *
 * @author Jay
 */
public class fillOne implements Runnable {

    DraftManager draftManager;
    WDK_GUI gui;
    DraftData draft;

    public fillOne(DraftManager draftManager, WDK_GUI gui) {
        this.draftManager = draftManager;
        this.gui = gui;
        this.draft = draftManager.getDraft();
    }

    public void run() {
        ObservableList<Player> playerList = draft.getFreeAgents().getPlayerList();
        for (Team team : draft.getFantasyTeams()) {
            {
                while (team.positionAvailable() != -1) {
                    int positionAvailable = team.positionAvailable();
                    String positionLooking;
                    if (positionAvailable == 0) {
                        positionLooking = "C";
                    } else if (positionAvailable == 1) {
                        positionLooking = "1B";
                    } else if (positionAvailable == 2) {
                        positionLooking = "3B";
                    } else if (positionAvailable == 3) {
                        positionLooking = "CI";
                    } else if (positionAvailable == 4) {
                        positionLooking = "2B";
                    } else if (positionAvailable == 5) {
                        positionLooking = "SS";
                    } else if (positionAvailable == 6) {
                        positionLooking = "MI";
                    } else if (positionAvailable == 7) {
                        positionLooking = "OF";
                    } else if (positionAvailable == 8) {
                        positionLooking = "U";
                    } else if (positionAvailable == 9) {
                        positionLooking = "P";
                    } else {
                        positionLooking = "Q";
                    }
                    int random = (int) (Math.random() * (playerList.size() - 0));
                    Player get = playerList.get(random);
                    while (!get.getPositionsList().contains(positionLooking)) {
                        random = (int) (Math.random() * (playerList.size() - 0));
                        get = playerList.get(random);
                    }
                    get.setCurrentPos(positionLooking);
                    get.setContract(Contract.S2);
                    get.setSalary(1);
                    get.setRefToCurrentTeam(team);
                    team.getPlayerList().add(get);

                    team.countTeam();
                    team.updateTeamStats();
                    playerList.remove(get);
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(fillOne.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    gui.addPlayerDraft();
                    gui.getTable5().setItems(draft.getDraftedPlayers());
                }

            }
            Thread.currentThread().destroy();
        }
    }
}
