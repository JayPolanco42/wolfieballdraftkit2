/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.concurrent.locks.Lock;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import wdk.file.DraftFileManager;
import wdkcontroller.DraftFileController;
import wdkcontroller.PlayerEditController;
import wdkcontroller.TeamEditController;
import wdkdata.Contract;
import wdkdata.DraftData;
import wdkdata.DraftManager;
import wdkdata.MLBTeams;
import wdkdata.Player;
import wdkdata.Team;
import static wolfieballdraftkit.WDK_Constants.PATH_CSS;
import static wolfieballdraftkit.WDK_Constants.PATH_IMAGES;
import wolfieballdraftkit.WDK_PropertyType;

/**
 *
 * @author Jay
 */
public class WDK_GUI {

    //CONSTONANTS FOR MANAGING STYLE
    static final String MAIN_STYLE_SHEET = PATH_CSS + "wdkStyleSheet.css";
    DraftData draft;

    Stage primaryStage;
    Scene primaryScene;
    BorderPane wdkPane;

    //THIS BELONGS TO THE TOP TOOLBAR THAT REMAINS ON SCENE
    HBox fileToolbarPane;
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exitButton;

    //This belongs to the Screen Toolbar
    HBox screenToolbarPane;
    Button playerScreenButton;
    Button fantasyTeamsScreenButton;
    Button fantasyStandingsButton;
    Button draftsScreenButton;
    Button mlbTeamsScreenButton;

    //This belongs to Player screen
    VBox playerPane;
    Label playerScreenHeading;

    HBox playerToolsWorkspace;
    Button addPlayerButton;
    Button removePlayerButton;
    Label playerSearchLabel;
    TextField searchPlayerTextField;

    //This belongs to the first HBox in Player Screen
    //These are the radiobuttons on the player screen
    HBox radioButtonPane;
    RadioButton allRadioButton;
    RadioButton cRadioButton;
    RadioButton clRadioButton;
    RadioButton b1RadioButton;
    RadioButton b3RadioButton;
    RadioButton b2RadioButton;
    RadioButton miRadioButton;
    RadioButton ssRadioButton;
    RadioButton ofRadioButton;
    RadioButton uRadioButton;
    RadioButton pRadioButton;

    Scene playerScene;
    //ALL ELEMENTS FOR FANTASY TEAMS SCREEN
    VBox fantasyTeamsPane;
    Label fantasyTeamsScreenHeading;

    HBox fantasyTeamsNameWorkspace;
    Label draftNameLabel;
    TextField draftNameTextField;

    HBox fantasyTeamsEditWorkspace;
    Button addFantasyTeamButton;
    Button removeFantasyTeamButton;
    Button editFantasyTeamButton;
    Label selectFantasyTeamLabel;
    ComboBox selectFantasyTeamComboBox;

    VBox fantasyTeamsTableWorkspace;
    Label startingLineupLabel;
    TableView<Player> table2;

    //ALL ELEMENTS FOR FANTASY STANDINGS SCREEN
    VBox fantasyStandingsPane;
    Label fantasyStandingsScreenHeading;
    TableView table4;
    TableColumn column50;
    TableColumn column51;
    TableColumn column52;
    TableColumn column53;
    TableColumn column54;
    TableColumn column55;
    TableColumn column56;
    TableColumn column57;
    TableColumn column58;
    TableColumn column59;
    TableColumn column60;
    TableColumn column61;
    TableColumn column62;
    TableColumn column63;
    TableColumn column64;
    TableColumn column65;

    //ALL ELEMENTS FOR DRAFT SCREEN
    VBox draftPane;
    Label draftScreenHeading;
    HBox actionButtons;
    Button starButton;
    Button playButton;
    Button pauseButton;
    private TableView table5;
    TableColumn column70;
    TableColumn column71;
    TableColumn column72;
    TableColumn column73;
    TableColumn column74;
    TableColumn column75;

    //ALL ELEMENTS FOR MLB SCREEN
    VBox mlbPane;
    Label mlbScreenHeading;
    HBox selectionPane;
    Label selectProTeamLabel;
    ComboBox selectProTeamComboBox;
    TableView<Player> table3;
    TableColumn column40;
    TableColumn column41;
    TableColumn column42;

    TableView<Player> table1;
    TableColumn column12;
    TableColumn column11;
    TableColumn column10;
    TableColumn column9;
    TableColumn column8;
    TableColumn column7;
    TableColumn column6;
    TableColumn column5;
    TableColumn column4;
    TableColumn column3;
    TableColumn column2;
    TableColumn column1;

    ObservableList<Player> filteredlist;
    //Controls all file handling such as New Draft, Load Draft etc..
    DraftFileController draftFileController;

    DraftManager draftManager;
    DraftFileManager draftFileManager;
    //Alert Dialog
    AlertDialog alertDialog;
    YesNoCancelDialog yesnocanceldialog;

    //Everything for fantasy Teams Screen
    TableColumn column20;
    TableColumn column21;
    TableColumn column22;
    TableColumn column23;
    TableColumn column24;
    TableColumn column25;
    TableColumn column26;
    TableColumn column27;
    TableColumn column28;
    TableColumn column29;
    TableColumn column30;
    TableColumn column31;
    TableColumn column32;

    Team availablePlayers;

    public WDK_GUI(Stage primaryStage) {
        draftManager = new DraftManager();
        availablePlayers = new Team("Free Agents", "Available Players");
        draftManager.getDraft().setFreeAgents(availablePlayers);
        draft = draftManager.getDraft();
        this.primaryStage = primaryStage;
    }

    public Stage getStage() {
        return primaryStage;
    }

    public void initStage() {
        primaryStage.setTitle("Wolfieball Draft Kit");

        Rectangle2D bounds = Screen.getPrimary().getBounds();

        //Sizing the window
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        wdkPane = new BorderPane();
        initToolbar();
        initFirstEventHandlers();
        wdkPane.setTop(fileToolbarPane);
        primaryScene = new Scene(wdkPane);
        primaryScene.getStylesheets().add(MAIN_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        initFileController();

    }

    private void initFileController() {
        initDialogs();
        draftFileController = new DraftFileController(alertDialog, draftFileManager);
    }

    private void initDialogs() {
        alertDialog = new AlertDialog(primaryStage, "Okay");
        yesnocanceldialog = new YesNoCancelDialog(primaryStage);

    }

    private void initScreens() {
        //initializes player screen
        initPlayerScreen();
        //initializes fantasy teams screen
        initFantasyTeamScreen();
        //initializes fantasy standing screen
        initFantasyStandingsScreen();
        //initializes draft screen
        initDraftScreen();
        //initializes mlb teams screen
        initMLBScreen();
    }

    public void initPlayerScreen() {

        playerPane = new VBox(10);
        playerToolsWorkspace = new HBox(10);
        radioButtonPane = new HBox(10);

        playerScreenHeading = initLabel(WDK_PropertyType.PLAYER_HEADING_LABEL, "heading_label");

        addPlayerButton = initChildButton("Add", "editTableButton", WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removePlayerButton = initChildButton("Remove", "editTableButton", WDK_PropertyType.REMOVE_ICON, WDK_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
        playerSearchLabel = initLabel(WDK_PropertyType.SEARCH_LABEL, "textLabel");
        searchPlayerTextField = initTextField("textField", "", 10, true);
        playerToolsWorkspace.getChildren().addAll(addPlayerButton, removePlayerButton, playerSearchLabel, searchPlayerTextField);

        playerToolsWorkspace.setAlignment(Pos.CENTER);
        HBox.setHgrow(searchPlayerTextField, Priority.ALWAYS);//Expands textfield to take remaining space
        playerPane.setId("toolPane");

        allRadioButton = initRadioButton("All", "radioButton");
        cRadioButton = initRadioButton("C", "radioButton");
        clRadioButton = initRadioButton("CI", "radioButton");
        b1RadioButton = initRadioButton("1B", "radioButton");
        b3RadioButton = initRadioButton("3B", "radioButton");
        b2RadioButton = initRadioButton("2B", "radioButton");
        miRadioButton = initRadioButton("MI", "radioButton");
        ssRadioButton = initRadioButton("SS", "radioButton");
        ofRadioButton = initRadioButton("OF", "radioButton");
        uRadioButton = initRadioButton("U", "radioButton");
        pRadioButton = initRadioButton("P", "radioButton");
        radioButtonPane.getChildren().addAll(allRadioButton, cRadioButton, clRadioButton, b1RadioButton,
                b3RadioButton, b2RadioButton, miRadioButton, ssRadioButton, ofRadioButton, uRadioButton, pRadioButton);
        radioButtonPane.setId("blueBar");

        table1 = new TableView();
        table1.setStyle("tableStyle");
        column1 = new TableColumn("First");
        column2 = new TableColumn("Last");
        column3 = new TableColumn("Pro Team");
        column4 = new TableColumn("Year of Birth");
        column5 = new TableColumn("Positions");
        column6 = new TableColumn("R/W");
        column7 = new TableColumn("HR/SV");
        column8 = new TableColumn("RBI/K");
        column9 = new TableColumn("SB/ERA");
        column10 = new TableColumn("BA/WHIP");
        column11 = new TableColumn("Estimated Value");
        column12 = new TableColumn("Notes");

        column1.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        column2.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        column3.setCellValueFactory(new PropertyValueFactory<>("proTeam"));
        column4.setCellValueFactory(new PropertyValueFactory<>("birthYear"));
        column5.setCellValueFactory(new PropertyValueFactory<>("positionsList"));
        column6.setCellValueFactory(new PropertyValueFactory<>("RW"));
        column7.setCellValueFactory(new PropertyValueFactory<>("hrSV"));
        column8.setCellValueFactory(new PropertyValueFactory<>("rbiK"));
        column9.setCellValueFactory(new PropertyValueFactory<>("sbEra"));
        column9.setComparator(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                double a = Double.valueOf(o1.toString());
                double b = Double.valueOf(o2.toString());
                if (a == b) {
                    return 0;
                }
                if (a > b) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });
        column10.setCellValueFactory(new PropertyValueFactory<>("baWhip"));
        column11.setCellValueFactory(new PropertyValueFactory<>("estimatedValue"));
        column12.setCellValueFactory(new PropertyValueFactory<>("notes"));
        column12.setCellFactory(TextFieldTableCell.forTableColumn());
        filteredlist = FXCollections.observableArrayList();

        table1.setEditable(true);
        table1.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table1.getColumns().addAll(column1, column2, column3, column4, column5, column6, column7, column8, column9, column10, column11, column12);
        playerPane.getChildren().addAll(playerScreenHeading, playerToolsWorkspace, radioButtonPane, table1);//ADDING TOP LABEL

    }

    public void initWorkSpace() {
        initScreens();
        showScreen(playerPane);
        initScreenbar();
        initOtherEventHandlers();
    }

    public void initFantasyTeamScreen() {
        fantasyTeamsPane = new VBox(10);
        fantasyTeamsPane.setId("toolPane");

        fantasyTeamsNameWorkspace = new HBox(10);
        draftNameLabel = initLabel(WDK_PropertyType.DRAFT_NAME_LABEL, "textLabel");
        draftNameTextField = initTextField("textField", "", 10, true);
        fantasyTeamsNameWorkspace.getChildren().addAll(draftNameLabel, draftNameTextField);

        fantasyTeamsEditWorkspace = new HBox(10);
        addFantasyTeamButton = initChildButton("Add", "editTableButton", WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_FANTASYTEAM_TOOLTIP, false);
        removeFantasyTeamButton = initChildButton("Remove", "editTableButton", WDK_PropertyType.REMOVE_ICON, WDK_PropertyType.REMOVE_FANTASYTEAM_TOOLTIP, false);
        editFantasyTeamButton = initChildButton("Edit", "editTableButton", WDK_PropertyType.EDIT_ICON, WDK_PropertyType.EDIT_FANTASYTEAM_TOOLTIP, false);
        selectFantasyTeamLabel = initLabel(WDK_PropertyType.FANTASYTEAMS_SELECT_LABEL, "textLabel");
        selectFantasyTeamComboBox = new ComboBox();
        selectFantasyTeamComboBox.setItems(draft.getFantasyTeams());

        fantasyTeamsEditWorkspace.getChildren().addAll(addFantasyTeamButton, removeFantasyTeamButton, editFantasyTeamButton, selectFantasyTeamLabel, selectFantasyTeamComboBox);

        fantasyTeamsTableWorkspace = new VBox(10);
        startingLineupLabel = initLabel(WDK_PropertyType.FANTASYTEAMS_LINEUP_LABEL, "textLabel");
        table2 = new TableView();
        fantasyTeamsTableWorkspace.setId("blueBar");
        fantasyTeamsTableWorkspace.getChildren().addAll(startingLineupLabel, table2);

        fantasyTeamsScreenHeading = initLabel(WDK_PropertyType.FANTASYTEAMS_HEADING_LABEL, "heading_label");
        fantasyTeamsPane.getChildren().addAll(fantasyTeamsScreenHeading, fantasyTeamsNameWorkspace, fantasyTeamsEditWorkspace, fantasyTeamsTableWorkspace);

        column20 = new TableColumn("Position");
        column21 = new TableColumn("First");
        column22 = new TableColumn("Last");
        column23 = new TableColumn("Pro Team");
        column24 = new TableColumn("Positions");
        column25 = new TableColumn("R/W");
        column26 = new TableColumn("HR/SV");
        column27 = new TableColumn("RBI/K");
        column28 = new TableColumn("SB/ERA");
        column29 = new TableColumn("BA/WHIP");
        column30 = new TableColumn("Estimated Value");
        column31 = new TableColumn("Contract");
        column32 = new TableColumn("Salary");

        column20.setCellValueFactory(new PropertyValueFactory<>("currentPos"));
        column21.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        column22.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        column23.setCellValueFactory(new PropertyValueFactory<>("proTeam"));
        column24.setCellValueFactory(new PropertyValueFactory<>("positionsList"));
        column25.setCellValueFactory(new PropertyValueFactory<>("RW"));
        column26.setCellValueFactory(new PropertyValueFactory<>("hrSV"));
        column27.setCellValueFactory(new PropertyValueFactory<>("rbiK"));
        column28.setCellValueFactory(new PropertyValueFactory<>("sbEra"));
        column29.setCellValueFactory(new PropertyValueFactory<>("baWhip"));
        column30.setCellValueFactory(new PropertyValueFactory<>("estimatedValue"));
        column31.setCellValueFactory(new PropertyValueFactory<>("contract"));
        column32.setCellValueFactory(new PropertyValueFactory<>("salary"));
        table2.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table2.getColumns().addAll(column20, column21, column22, column23, column24, column25, column26, column27, column28, column29, column30, column31, column32);
    }

    public void initFantasyStandingsScreen() {
        fantasyStandingsPane = new VBox(10);
        fantasyStandingsPane.setId("toolPane");
        fantasyStandingsScreenHeading = initLabel(WDK_PropertyType.FANTASYSTANDINGS_HEADING_LABEL, "heading_label");
        fantasyStandingsPane.getChildren().add(fantasyStandingsScreenHeading);

        table4 = new TableView();
        column50 = new TableColumn("Fantasy Team Name");
        column51 = new TableColumn("Players Needed");
        column52 = new TableColumn("$ Left");
        column53 = new TableColumn("$ PP");
        column54 = new TableColumn("R");
        column55 = new TableColumn("HR");
        column56 = new TableColumn("RBI");
        column57 = new TableColumn("SB");
        column58 = new TableColumn("BA");
        column59 = new TableColumn("W");
        column60 = new TableColumn("SV");
        column61 = new TableColumn("K");
        column62 = new TableColumn("ERA");
        column63 = new TableColumn("WHIP");
        column64 = new TableColumn("Total Points");
        column50.setCellValueFactory(new PropertyValueFactory<>("teamName"));
        column51.setCellValueFactory(new PropertyValueFactory<>("playerSpaces"));
        column52.setCellValueFactory(new PropertyValueFactory<>("moneyLeft"));
        column53.setCellValueFactory(new PropertyValueFactory<>("moneyPerPlayer"));
        column54.setCellValueFactory(new PropertyValueFactory<>("rSum"));
        column55.setCellValueFactory(new PropertyValueFactory<>("hrSum"));
        column56.setCellValueFactory(new PropertyValueFactory<>("rbiSum"));
        column57.setCellValueFactory(new PropertyValueFactory<>("sbSum"));
        column58.setCellValueFactory(new PropertyValueFactory<>("baAvg"));
        column59.setCellValueFactory(new PropertyValueFactory<>("wSum"));
        column60.setCellValueFactory(new PropertyValueFactory<>("svSum"));
        column61.setCellValueFactory(new PropertyValueFactory<>("kSum"));
        column62.setCellValueFactory(new PropertyValueFactory<>("eraAvg"));
        column63.setCellValueFactory(new PropertyValueFactory<>("whipAvg"));
        column64.setCellValueFactory(new PropertyValueFactory<>("totalPoints"));
        column50.setMinWidth(75);
        column51.setMinWidth(75);
        table4.getColumns().addAll(column50, column51, column52, column53, column54, column55, column56, column57, column58, column59, column60, column61, column62, column63, column64);
        table4.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        fantasyStandingsPane.getChildren().add(table4);
        table4.setItems(draft.getFantasyTeams());

    }

    public void initDraftScreen() {
        draftPane = new VBox(10);
        draftPane.setId("toolPane");
        draftScreenHeading = initLabel(WDK_PropertyType.DRAFT_HEADING_LABEL, "heading_label");
        draftPane.getChildren().add(draftScreenHeading);

        actionButtons = new HBox(10);
        starButton = initChildButton("", "editTableButton", WDK_PropertyType.STAR_ICON, WDK_PropertyType.STAR_TOOLTIP, false);
        playButton = initChildButton("", "editTableButton", WDK_PropertyType.PLAY_ICON, WDK_PropertyType.PLAY_TOOLTIP, false);
        pauseButton = initChildButton("", "editTableButton", WDK_PropertyType.PAUSE_ICON, WDK_PropertyType.PAUSE_TOOLTIP, false);

        setTable5(new TableView());
        column70 = new TableColumn("Pick #");
        column71 = new TableColumn("First");
        column72 = new TableColumn("Last");
        column73 = new TableColumn("Team");
        column74 = new TableColumn("Contract");
        column75 = new TableColumn("Salary");
        column70.setCellValueFactory(new Callback<CellDataFeatures<Player, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<Player, String> p) {
                return new ReadOnlyObjectWrapper(getTable5().getItems().indexOf(p.getValue()) + 1 + "");
            }
        });
        column70.setSortable(false);

        TableColumn<Player, Number> indexColumn = new TableColumn<Player, Number>("#");
        indexColumn.setSortable(false);
        indexColumn.setCellValueFactory(column -> new ReadOnlyObjectWrapper<Number>(getTable5().getItems().indexOf(column.getValue())));

        column71.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        column72.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        column73.setCellValueFactory(new PropertyValueFactory<>("refToCurrentTeam"));
        column74.setCellValueFactory(new PropertyValueFactory<>("contract"));
        column75.setCellValueFactory(new PropertyValueFactory<>("salary"));
        getTable5().setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        getTable5().setMaxWidth(500);

        getTable5().getColumns().addAll(column70, column71, column72, column73, column74, column75);
        actionButtons.getChildren().addAll(starButton, playButton, pauseButton);
        draftPane.getChildren().addAll(actionButtons, getTable5());

    }

    public void initMLBScreen() {
        mlbPane = new VBox(10);
        mlbPane.setId("toolPane");

        selectionPane = new HBox(10);
        selectProTeamLabel = initLabel(WDK_PropertyType.MLBTEAMS_SELECTION_LABEL, "textLabel");
        mlbScreenHeading = initLabel(WDK_PropertyType.MLBTEAMS_HEADING_LABEL, "heading_label");
        selectProTeamComboBox = new ComboBox();
        selectProTeamComboBox.setItems(FXCollections.observableArrayList(MLBTeams.values()));
        selectionPane.getChildren().addAll(selectProTeamLabel, selectProTeamComboBox);

        table3 = new TableView<Player>();

        column40 = new TableColumn("First Name");
        column41 = new TableColumn("Last Name");
        column42 = new TableColumn("Positions");
        column40.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        column41.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        column42.setCellValueFactory(new PropertyValueFactory<>("positionsList"));
        table3.setMaxWidth(400);

        table3.getColumns().addAll(column40, column41, column42);
        mlbPane.getChildren().addAll(mlbScreenHeading, selectionPane, table3);

    }

    public void initFirstEventHandlers() {
        newDraftButton.setOnAction(e -> {
            initWorkSpace();
            draftFileController.doNewDraftRequest(this);
            resetTable();
        });

        saveDraftButton.setOnAction(e -> {
            draftFileManager.doSaveCourseRequest(this, draft);
        });
    }

    public void initOtherEventHandlers() {
        playerScreenButton.setOnAction(e -> {
            showScreen(playerPane);
            table1.setVisible(false);
            table1.setVisible(true);
        });
        fantasyTeamsScreenButton.setOnAction(e -> {
            showScreen(fantasyTeamsPane);
            table2.setVisible(false);
            table2.setVisible(true);
        });
        fantasyStandingsButton.setOnAction(e -> {
            showScreen(fantasyStandingsPane);
            table4.getColumns().clear();
            table4.getColumns().addAll(column50, column51, column52, column53, column54, column55, column56, column57, column58, column59, column60, column61, column62, column63, column64);

        });
        draftsScreenButton.setOnAction(e -> {
            showScreen(draftPane);
            addPlayerDraft();
            getTable5().setItems(draft.getDraftedPlayers());
        });
        mlbTeamsScreenButton.setOnAction(e -> {
            showScreen(mlbPane);
        });
        allRadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            seeAll();
            columnsAll();
        });
        cRadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (cRadioButton.isSelected()) {
                columnsHitters();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        clRadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (clRadioButton.isSelected()) {
                columnsHitters();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        b1RadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (b1RadioButton.isSelected()) {
                columnsHitters();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        b3RadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (b3RadioButton.isSelected()) {
                columnsHitters();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        b2RadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (b2RadioButton.isSelected()) {
                columnsHitters();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        miRadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (miRadioButton.isSelected()) {
                columnsHitters();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        ssRadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (ssRadioButton.isSelected()) {
                columnsHitters();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        ofRadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (ofRadioButton.isSelected()) {
                columnsHitters();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        uRadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (uRadioButton.isSelected()) {
                columnsHitters();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });
        pRadioButton.setOnAction(e -> {
            filterList(searchPlayerTextField.getText().toLowerCase());
            if (pRadioButton.isSelected()) {
                columnsPitchers();
            } else {
                if (isHitterSelected()) {
                    columnsHitters();
                } else {
                    columnsAll();
                }
            }
        });

        searchPlayerTextField.setOnKeyReleased(e -> {
            String searchString = searchPlayerTextField.getText().toLowerCase();
            filterList(searchString);
        });
        column12.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Player, String>>() {

                    @Override
                    public void handle(TableColumn.CellEditEvent<Player, String> t) {
                        ((Player) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())).setNotes(t.getNewValue());
                    }
                });
        PlayerEditController playerController = new PlayerEditController(primaryStage, draft, alertDialog, yesnocanceldialog);

        addPlayerButton.setOnAction(e -> {
            playerController.handleAddPlayer(this);
            resetTable();
        });
        removePlayerButton.setOnAction(e -> {
            playerController.handleRemovePlayerRequest(this, table1.getSelectionModel().getSelectedItem());
            resetTable();
        });
        table1.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                playerController.handleEditPlayerRequest(this, table1.getSelectionModel().getSelectedItem());
            }
        });
        TeamEditController teamController = new TeamEditController(primaryStage, draft, alertDialog, yesnocanceldialog);
        addFantasyTeamButton.setOnAction(e -> {
            teamController.handleAddTeamRequest(this);
        });
        removeFantasyTeamButton.setOnAction(e -> {
            teamController.handleRemoveTeamRequest(this, (Team) selectFantasyTeamComboBox.getSelectionModel().getSelectedItem());
        });
        editFantasyTeamButton.setOnAction(e -> {
            Team selectedTeam = (Team) selectFantasyTeamComboBox.getSelectionModel().getSelectedItem();
            teamController.handleEditTeamRequest(this, selectedTeam);
        });
        selectFantasyTeamComboBox.setOnAction(e -> {
            if (!selectFantasyTeamComboBox.getSelectionModel().isEmpty()) {
                Team selectedTeam = (Team) selectFantasyTeamComboBox.getSelectionModel().getSelectedItem();
                table2.setItems(selectedTeam.getPlayerList());
            }
        });
        table2.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                if (table2.getSelectionModel().getSelectedItem() != null) {//TODO
                    playerController.handleEditPlayerRequest(this, table2.getSelectionModel().getSelectedItem());
                }
            }
        });
        draftNameTextField.setOnKeyReleased(e -> {
            draft.setTitleOfDraft(draftNameTextField.getText());
        });
        selectProTeamComboBox.setOnAction(e -> {
            ObservableList<Player> list = FXCollections.observableArrayList();
            for (int i = 0; i < draft.getFantasyTeams().size(); i++) {
                for (int j = 0; j < draft.getFantasyTeams().get(i).getPlayerList().size(); j++) {
                    if (draft.getFantasyTeams().get(i).getPlayerList().get(j).getProTeam().equals((MLBTeams) selectProTeamComboBox.getSelectionModel().getSelectedItem())) {
                        list.add(draft.getFantasyTeams().get(i).getPlayerList().get(j));
                    }
                }
            }
            for (Player player : draft.getFreeAgents().getPlayerList()) {
                if (player.getProTeam().equals((MLBTeams) selectProTeamComboBox.getSelectionModel().getSelectedItem())) {
                    list.add(player);
                }
            }
            table3.setItems(list);
            column42.setSortType(TableColumn.SortType.ASCENDING);
        });
        starButton.setOnAction(e -> {
            fillSpot();
            addPlayerDraft();
            getTable5().setItems(draft.getDraftedPlayers());
            getTable5().getColumns().clear();
            getTable5().getColumns().addAll(column70, column71, column72, column73, column74);
        }); 
        Runnable doIt = new fillOne(draftManager, this);
            Thread thread = new Thread(doIt);
        playButton.setOnAction(e->{
            thread.start();
                  });
        pauseButton.setOnAction(e->{
            if(thread.isAlive()){
                thread.suspend();
            }
        });

    }
   
    public void addPlayerDraft() {
        draft.getDraftedPlayers().clear();
        for (Team team : draft.getFantasyTeams()) {
            for (Player player : team.getPlayerList()) {
                if (player.getContract() == Contract.S2) {
                    draft.getDraftedPlayers().add(player);
                }
            }
        }
    }

    public void fillSpot() {
        ObservableList<Player> playerList = draft.getFreeAgents().getPlayerList();
        for (Team team : draft.getFantasyTeams()) {
            {
                if (team.positionAvailable() != -1 ) {
                    int positionAvailable = team.positionAvailable();
                    String positionLooking;
                    if (positionAvailable == 0) {
                        positionLooking = "C";
                    } else if (positionAvailable == 1) {
                        positionLooking = "1B";
                    } else if (positionAvailable == 2) {
                        positionLooking = "3B";
                    } else if (positionAvailable == 3) {
                        positionLooking = "CI";
                    } else if (positionAvailable == 4) {
                        positionLooking = "2B";
                    } else if (positionAvailable == 5) {
                        positionLooking = "SS";
                    } else if (positionAvailable == 6) {
                        positionLooking = "MI";
                    } else if (positionAvailable == 7) {
                        positionLooking = "OF";
                    } else if (positionAvailable == 8) {
                        positionLooking = "U";
                    } else if (positionAvailable == 9) {
                        positionLooking = "P";
                    } else {
                        positionLooking = "Q";
                    }
                    int random = (int) (Math.random() * (playerList.size() - 0));
                    Player get = playerList.get(random);
                    while (!get.getPositionsList().contains(positionLooking)) {
                        random = (int) (Math.random() * (playerList.size() - 0));
                        get = playerList.get(random);
                    }
                    get.setCurrentPos(positionLooking);
                    get.setContract(Contract.S2);
                    get.setSalary(1);
                    get.setRefToCurrentTeam(team);
                    team.getPlayerList().add(get);
                    
                    team.countTeam();
                    team.updateTeamStats();
                    playerList.remove(get);
                }
                


            }
        }
    }

    public boolean isHitterSelected() {
        if (cRadioButton.isSelected()
                || clRadioButton.isSelected()
                || b1RadioButton.isSelected()
                || b3RadioButton.isSelected()
                || b2RadioButton.isSelected()
                || miRadioButton.isSelected()
                || ssRadioButton.isSelected()
                || ofRadioButton.isSelected()
                || uRadioButton.isSelected()) {
            return true;
        } else {
            return false;
        }
    }

    public void columnsAll() {
        column6.setText("R/W");
        column7.setText("HR/SV");
        column8.setText("RBI/ERA");
        column9.setText("SB/ERA");
        column10.setText("BA/WHIP");

    }

    public void columnsHitters() {
        column6.setText("R");
        column7.setText("HR");
        column8.setText("RBI");
        column9.setText("SB");
        column10.setText("BA");
    }

    public void columnsPitchers() {
        column6.setText("W");
        column7.setText("SV");
        column8.setText("K");
        column9.setText("ERA");
        column10.setText("WHIP");
    }

    public void seeAll() {
        for (int i = 1; i < radioButtonPane.getChildren().size(); i++) {
            RadioButton currentButton = (RadioButton) radioButtonPane.getChildren().get(i);
            currentButton.setSelected(false);
        }
    }

    public void deselectAllButtons(int start, int end) {
        for (int i = start; i < end; i++) {
            RadioButton currentButton = (RadioButton) radioButtonPane.getChildren().get(i);
            currentButton.setSelected(false);
        }
    }

    public ObservableList<Player> filterList(String searchString) {
        filteredlist.clear();

        for (int i = 0; i < availablePlayers.getPlayerList().size(); i++) {
            String firstName = availablePlayers.getPlayerList().get(i).getFirstName().toLowerCase();
            String lastName = availablePlayers.getPlayerList().get(i).getLastName().toLowerCase();
            if (firstName.startsWith(searchString) || lastName.startsWith(searchString)) {
                filteredlist.add(availablePlayers.getPlayerList().get(i));
            }
        }
        ObservableList<Player> removeList = FXCollections.observableArrayList();
        for (int j = 0; j < radioButtonPane.getChildren().size(); j++) {
            RadioButton currentButton = (RadioButton) radioButtonPane.getChildren().get(j);
            if (currentButton.isSelected()) {
                if (currentButton == allRadioButton) {
                    deselectAllButtons(1, 11);
                    continue;
                }
                //Add conditions for all other buttons
                for (int i = 0; i < filteredlist.size(); i++) {
                    Player currentPlayer = filteredlist.get(i);
                    if (currentButton == clRadioButton) {
                        if (!(checkPos(currentPlayer.getPositionsList(), "1B") || checkPos(currentPlayer.getPositionsList(), "3B"))) {
                            removeList.add(currentPlayer);
                        }
                    } else if (currentButton == miRadioButton) {
                        if (!(checkPos(currentPlayer.getPositionsList(), "2B") || checkPos(currentPlayer.getPositionsList(), "SS"))) {
                            removeList.add(currentPlayer);
                        }
                    } else if (currentButton == uRadioButton) {
                        if (checkPos(currentPlayer.getPositionsList(), "P")) {
                            removeList.add(currentPlayer);
                        }
                    } else if (!checkPos(currentPlayer.getPositionsList(), currentButton.getText())) {
                        removeList.add(currentPlayer);
                    }

                }
            }
        }
        filteredlist.removeAll(removeList);
        table1.setItems(filteredlist);
        return filteredlist;
    }

    public boolean checkPos(ObservableList<String> list, String pos) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(pos)) {
                return true;
            }
        }
        return false;
    }

    public void resetTable() {
        filteredlist.clear();
        filteredlist.addAll(availablePlayers.getPlayerList());
        table1.setItems(filteredlist);
    }

    //Constructs TOP Toolbar
    private void initToolbar() {
        fileToolbarPane = new HBox();
        newDraftButton = initChildButton("New", "toolbarButton", WDK_PropertyType.NEW_DRAFT_ICON, WDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        loadDraftButton = initChildButton("Load", "toolbarButton", WDK_PropertyType.LOAD_DRAFT_ICON, WDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        saveDraftButton = initChildButton("Save", "toolbarButton", WDK_PropertyType.SAVE_DRAFT_ICON, WDK_PropertyType.SAVE_DRAFT_TOOLTIP, false);
        exitButton = initChildButton("Exit", "toolbarButton", WDK_PropertyType.EXIT_ICON, WDK_PropertyType.EXIT_TOOLTIP, false);
        fileToolbarPane.getChildren().addAll(newDraftButton, loadDraftButton, saveDraftButton, exitButton);
        fileToolbarPane.setPadding(new Insets(20, 20, 20, 20));
        fileToolbarPane.setSpacing(18);
        fileToolbarPane.setId("blueBar");
        fileToolbarPane.setAlignment(Pos.BASELINE_LEFT);

    }

    //Constructs Screen Buttons
    private void initScreenbar() {
        screenToolbarPane = new HBox();

        playerScreenButton = initChildButton("Players", "toolbarButton", WDK_PropertyType.PLAYERS_SCREEN_ICON, WDK_PropertyType.PLAYER_SCREEN_TOOLTIP, false);
        fantasyTeamsScreenButton = initChildButton("Fantasy Teams", "toolbarButton", WDK_PropertyType.FANTASYTEAMS_SCREEN_ICON, WDK_PropertyType.FANTASYTEAM_SCREEN_TOOLTIP, false);
        fantasyStandingsButton = initChildButton("Fantasy Standings", "toolbarButton", WDK_PropertyType.FANTASYSTANDINGS_SCREEN_ICON, WDK_PropertyType.FANTASYSTANDING_SCREEN_TOOLTIP, false);
        draftsScreenButton = initChildButton("Drafts", "toolbarButton", WDK_PropertyType.DRAFTS_SCREEN_ICON, WDK_PropertyType.DRAFT_SCREEN_TOOLTIP, false);
        mlbTeamsScreenButton = initChildButton("MLB Teams", "toolbarButton", WDK_PropertyType.MLBTEAMS_SCREEN_ICON, WDK_PropertyType.MLBTEAMS_SCREEN_TOOLTIP, false);
        screenToolbarPane.getChildren().addAll(playerScreenButton, fantasyTeamsScreenButton, fantasyStandingsButton, draftsScreenButton, mlbTeamsScreenButton);

        screenToolbarPane.setPadding(new Insets(20, 20, 20, 20));
        screenToolbarPane.setSpacing(18);
        screenToolbarPane.setId("blueBar");
        screenToolbarPane.setAlignment(Pos.BASELINE_LEFT);

        wdkPane.setBottom(screenToolbarPane);
    }

    private Button initChildButton(String buttonText, String styleID, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        ImageView imageView = new ImageView(buttonImage);
        button.setId(styleID);
        button.setGraphic(imageView);
        button.setText(buttonText);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        return button;
    }

    public TextField initTextField(String styleID, String initialText, int size, boolean editable) {
        TextField textField = new TextField();
        textField.setPrefColumnCount(size);
        textField.setText(initialText);
        textField.setId(styleID);
        textField.setEditable(editable);
        return textField;
    }

    public void showScreen(Pane pane) {
        wdkPane.setCenter(pane);
    }

    public void initGUI() {
        initStage(); //Initializes the stage
    }

    public Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label((labelText));
        label.getStyleClass().add(styleClass);
        return label;
    }

    public RadioButton initRadioButton(String radioButtonLabel, String styleID) {
        RadioButton radioButton = new RadioButton(radioButtonLabel);
        radioButton.setId(styleID);
        radioButton.setPadding(new Insets(20, 20, 20, 20));
        return radioButton;
    }

    /**
     * @return the draftFileManager
     */
    public DraftFileManager getDraftFileManager() {
        return draftFileManager;
    }

    /**
     * @param draftFileManager the draftFileManager to set
     */
    public void setDraftFileManager(DraftFileManager draftFileManager) {
        this.draftFileManager = draftFileManager;
    }

    /**
     * @return the draft
     */
    public DraftData getDraft() {
        return draft;
    }

    /**
     * @param draft the draft to set
     */
    public void setDraft(DraftData draft) {
        this.draft = draft;
    }

    public DraftManager getDraftManager() {
        return draftManager;
    }

    /**
     * @return the table5
     */
    public TableView getTable5() {
        return table5;
    }

    /**
     * @param table5 the table5 to set
     */
    public void setTable5(TableView table5) {
        this.table5 = table5;
    }

}
