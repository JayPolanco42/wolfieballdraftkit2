/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static wdk.gui.WDK_GUI.MAIN_STYLE_SHEET;
import wdkdata.DraftData;
import wdkdata.Team;

/**
 *
 * @author Jay
 */
public class TeamDialog extends Stage {

    DraftData draft;
    Scene dialogScene;
    VBox addTeamPane;
    Label fantasyTeamDetailsHeading;
    Label nameLabel;
    TextField nameTextField;
    Label ownerLabel;
    TextField ownerTextField;
    HBox namePane;
    HBox ownerPane;
    Button completeButton;
    Button cancelButton;
    HBox buttonPane;
    Team team;
    String selection;

    public TeamDialog(Stage primaryStage, DraftData draft, AlertDialog alert) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        this.draft = draft;
        addTeamPane = new VBox(20);
        namePane = new HBox(10);
        ownerPane = new HBox(10);
        buttonPane = new HBox(10);
        buttonPane.setPadding(new Insets(10, 10, 10, 10));
        addTeamPane.setId("blueBar");
        fantasyTeamDetailsHeading = new Label("Fantasy Team Details");
        nameLabel = new Label("Name: ");
        nameLabel.setId("dialogTextLabel");
        nameTextField = new TextField();
        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            team.setTeamName(nameTextField.getText());
        });
        namePane.getChildren().addAll(nameLabel, nameTextField);

        ownerLabel = new Label("Owner: ");
        ownerLabel.setId("dialogTextLabel");
        ownerTextField = new TextField();
        ownerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            team.setOwnerName(ownerTextField.getText());
        });
        ownerPane.getChildren().addAll(ownerLabel, ownerTextField);

        
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");
        buttonPane.getChildren().addAll(completeButton, cancelButton);
        
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button) ae.getSource();
            this.selection = sourceButton.getText();
            TeamDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);


        fantasyTeamDetailsHeading.getStyleClass().add("heading_label");
        addTeamPane.getChildren().addAll(fantasyTeamDetailsHeading, namePane, ownerPane, buttonPane);

        dialogScene = new Scene(addTeamPane);
        dialogScene.getStylesheets().add(MAIN_STYLE_SHEET);
        this.setScene(dialogScene);

    }

    public Team showAddTeamDialog() {
        setTitle("Add Team");
        team = new Team();
        loadGui();
        this.showAndWait();
        return team;
    }
    public void loadGui(){
        ownerTextField.setText(team.getOwnerName());
        nameTextField.setText(team.getTeamName());
    }
    public Team showEditTeamDialog(Team teamEd){
        setTitle("Edit Team");
        team = new Team();
        ownerTextField.setText(teamEd.getOwnerName());
        nameTextField.setText(teamEd.getTeamName());
        this.showAndWait();        
        team.setTeamName(nameTextField.getText());
        team.setOwnerName(ownerTextField.getText());
        return team;
    }

    public boolean wasCompletedSelected() {
        return selection.equals("Complete");
    }
}
