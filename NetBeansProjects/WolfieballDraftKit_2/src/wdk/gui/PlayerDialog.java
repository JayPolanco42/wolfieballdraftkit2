/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wdk.gui.WDK_GUI.MAIN_STYLE_SHEET;
import wdkdata.Contract;
import wdkdata.DraftData;
import wdkdata.Player;
import wdkdata.MLBTeams;
import wdkdata.Team;
import static wolfieballdraftkit.WDK_Constants.PATH_IMAGES;

/**
 *
 * @author Jay
 */
public class PlayerDialog extends Stage {

    Player player;
    DraftData draft;

    VBox addPlayerPane;
    HBox firstNamePane;
    HBox lastNamePane;
    HBox proTeamPane;
    HBox buttonPane;
    HBox buttonPane2;
    HBox positionPane;

    Scene dialogScene;

    Label headingLabel;
    Label headingLabel2;
    Label firstNameLabel;
    Label lastNameLabel;
    Label proTeamLabel;

    TextField firstNameField;
    TextField lastNameField;
    ComboBox proTeamComboBox;
    CheckBox cCheckBox;
    CheckBox b1CheckBox;
    CheckBox b3CheckBox;
    CheckBox b2CheckBox;
    CheckBox ssCheckBox;
    CheckBox ofCheckBox;
    CheckBox pCheckBox;
    Button completeButton;
    Button completeButton2;
    Button cancelButton;
    Button cancelButton2;
    Button moveBackButton;
    private String selection;

    VBox editPlayerPane;
    HBox imagePane;

    VBox flagPane;
    Label nameLabel;
    Label positionsLabel;

    HBox fantasyTeamPane;
    Label fantasyTeamLabel;
    private ComboBox fantasyTeamComboBox;

    HBox singlePositionPane;
    Label positionLabel;
    ComboBox positionComboBox;

    HBox contractPane;
    Label contractLabel;
    private ComboBox contractComboBox;

    HBox salaryPane;
    Label salaryLabel;
    private TextField salaryTextField;

    Image flagImage;
    ImageView flagImageView;
    Image image;
    ImageView playerImageView;
    private ObservableList<Player> destinationList;
    AlertDialog alert;
    Button moveToTaxiSquad;
    String oldPosition;

    public static final String COMPLETE = "COMPLETE";
    public static final String CANCEL = "CANCEL";

    public PlayerDialog(Stage primaryStage, DraftData draft, AlertDialog alert) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        this.draft = draft;
        addPlayerPane = new VBox(20);
        this.alert = alert;
       
        editPlayerPane = new VBox(20);
        editPlayerPane.setId("blueBar");
        flagPane = new VBox(20);
        imagePane = new HBox(20);

        fantasyTeamPane = new HBox(20);
        fantasyTeamLabel = new Label("Fantasy Teams: ");
        fantasyTeamComboBox = new ComboBox();
        fantasyTeamPane.getChildren().addAll(fantasyTeamLabel, fantasyTeamComboBox);
        fantasyTeamLabel.setId("dialogTextLabel");

        singlePositionPane = new HBox(20);
        positionLabel = new Label("Position: ");
        positionComboBox = new ComboBox();
        singlePositionPane.getChildren().addAll(positionLabel, positionComboBox);
        positionLabel.setId("dialogTextLabel");
        completeButton2 = new Button(COMPLETE);
        cancelButton2 = new Button(CANCEL);
        contractPane = new HBox(20);
        contractLabel = new Label("Contract: ");
        contractComboBox = new ComboBox();
        contractComboBox.getItems().addAll(Contract.values());
        contractPane.getChildren().addAll(contractLabel, contractComboBox);
        contractLabel.setId("dialogTextLabel");
        moveToTaxiSquad = new Button("Move to Taxi Squad");
        salaryPane = new HBox(20);
        salaryLabel = new Label("Salary: ");
        salaryTextField = new TextField();
        salaryLabel.setId("dialogTextLabel");
        salaryPane.getChildren().addAll(salaryLabel, salaryTextField);

        fantasyTeamComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            player.setRefToCurrentTeam((Team) fantasyTeamComboBox.getSelectionModel().getSelectedItem());
        });
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setSalary(Integer.parseInt(salaryTextField.getText()));
        });

        positionComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            player.setCurrentPos((String) positionComboBox.getSelectionModel().getSelectedItem());
        });
        contractComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            player.setContract((Contract) contractComboBox.getSelectionModel().getSelectedItem());
        });

        //TODO MOVE DECLARATION TO OUTSIDE
        image = getImage("players", "AAA_PhotoMissing", ".jpg");
        playerImageView = new ImageView(image);

        flagImage = getImage("flags", "USA", ".png");
        flagImageView = new ImageView(flagImage);
        nameLabel = new Label("Name");
        positionsLabel = new Label("Positions Here");
        positionsLabel.setId("dialogTextLabel");
        nameLabel.setId("dialogTextLabel");
        flagPane.getChildren().addAll(flagImageView, nameLabel, positionsLabel);

        imagePane.getChildren().addAll(playerImageView, flagPane);

        //MOVE EVERYTHING HERE ABOVE EDIT PLAYER PANE
        headingLabel = new Label("Player Details");
        headingLabel.getStyleClass().add("heading_label");
        headingLabel2 = new Label("Player Details");
        headingLabel2.getStyleClass().add("heading_label");

        firstNameLabel = new Label("First Name: ");
        firstNameLabel.setId("dialogTextLabel");
        lastNameLabel = new Label("Last Name: ");
        lastNameLabel.setId("dialogTextLabel");
        proTeamLabel = new Label("Pro Team: ");
        proTeamLabel.setId("dialogTextLabel");
        firstNameField = new TextField();
        lastNameField = new TextField();
        proTeamComboBox = new ComboBox();
        proTeamComboBox.getItems().setAll(MLBTeams.values());
        lastNamePane = new HBox();
        firstNamePane = new HBox(10);
        proTeamPane = new HBox();
        buttonPane = new HBox(20);
        buttonPane2 = new HBox(20);
        moveBackButton = new Button("BACK TO FREE AGENTS");
        buttonPane2.getChildren().addAll(completeButton2, cancelButton2, moveBackButton, moveToTaxiSquad);
        positionPane = new HBox(20);
        moveToTaxiSquad.setOnAction(e -> {
            if(player.getRefToCurrentTeam().sumArray()>0){
                alert.show("Please, fill up your starting line up before adding to taxi squad");
            }
            else if (player.getRefToCurrentTeam().countTaxi() == 8) {
                alert.show("You already have the maximum number of taxi players.");
            } else {
                selection = "taxi";
                player.setContract(Contract.X);
                player.setSalary(1);
                player.setCurrentPos("Taxi");
                this.hide();
            }
        });
        cCheckBox = new CheckBox("C");
        b1CheckBox = new CheckBox("B1");
        b2CheckBox = new CheckBox("B2");
        b3CheckBox = new CheckBox("B3");
        ssCheckBox = new CheckBox("SS");
        ofCheckBox = new CheckBox("OF");
        pCheckBox = new CheckBox("P");
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button) ae.getSource();
            PlayerDialog.this.selection = sourceButton.getText();
            if (this.wasCompletedSelected() && !checkValidity()) {

            } else {
                this.hide();
            }

        };
        EventHandler completeCancelHandler2 = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button) ae.getSource();
            PlayerDialog.this.selection = sourceButton.getText();
            this.hide();
        };
        completeButton.setOnAction(completeCancelHandler2);
        cancelButton.setOnAction(completeCancelHandler2);
        completeButton2.setOnAction(completeCancelHandler);
        cancelButton2.setOnAction(completeCancelHandler);
        moveBackButton.setOnAction(e -> {
            selection = "false";
            this.hide();
        });
        positionPane.getChildren().addAll(cCheckBox, b1CheckBox, b2CheckBox, b3CheckBox, ssCheckBox, ofCheckBox, pCheckBox);
        buttonPane.setPadding(new Insets(5, 5, 5, 5));
        firstNamePane.getChildren().addAll(firstNameLabel, firstNameField);
        lastNamePane.getChildren().addAll(lastNameLabel, lastNameField);
        proTeamPane.getChildren().addAll(proTeamLabel, proTeamComboBox);
        buttonPane.getChildren().addAll(completeButton, cancelButton);

        firstNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setFirstName(firstNameField.getText());
        });
        lastNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLastName(lastNameField.getText());
        });
        proTeamComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            player.setProTeam(proTeamComboBox.getSelectionModel().getSelectedItem().toString());
        });

        addPlayerPane.getChildren().addAll(headingLabel, firstNamePane, lastNamePane, proTeamPane, positionPane, buttonPane);
        dialogScene = new Scene(addPlayerPane);
        dialogScene.getStylesheets().add(MAIN_STYLE_SHEET);
        addPlayerPane.setId("blueBar");
        editPlayerPane.getChildren().addAll(headingLabel2, imagePane, fantasyTeamPane, singlePositionPane, contractPane, salaryPane, buttonPane2);

        this.setScene(dialogScene);
    }

    public Player showAddPlayer() {
        setTitle("Add Player");
        player = new Player();
        player.setRefToCurrentTeam(draft.getFreeAgents());
        resetFields();
        dialogScene.setRoot(addPlayerPane);
        this.showAndWait();
        for (int i = 0; i < positionPane.getChildren().size(); i++) {
            CheckBox currentCheckBox = (CheckBox) positionPane.getChildren().get(i);
            if (currentCheckBox.isSelected()) {
                player.getPositionsList().add(currentCheckBox.getText());
            }
        }
        if (pCheckBox.isSelected()) {
            player.setIsPitcher(true);
        }
        return player;
    }

    public Player showEditPlayer(Player player2) {
        setTitle("Edit Player");
        dialogScene.setRoot(editPlayerPane);
        if (player2.getRefToCurrentTeam() == draft.getFreeAgents()) {
            moveBackButton.setDisable(true);
        } else {
            moveBackButton.setDisable(false);
        }
        player = new Player();
        oldPosition = player2.getCurrentPos();
        setPlayerOne(player2);
        loadPlayer(player);
        this.showAndWait();

        return player;
        //TODO
    }

    public boolean checkValidity() {
        if (contractComboBox.getSelectionModel().isEmpty() || salaryTextField.getText().isEmpty()) {
            alert.show("Please select Salary and Contract");
            return false;
        }
        if (fantasyTeamComboBox.getSelectionModel().isEmpty()) {
            alert.show("Please select a fantasy team");
            return false;
        }
        if (positionComboBox.getSelectionModel().isEmpty()) {
            alert.show("Please select valid position.");
            return false;
        }
        player.getRefToCurrentTeam().countTeam();
        if (!player.getRefToCurrentTeam().spaceLeft(player) && !player.getCurrentPos().equals(oldPosition)) {
           
            alert.show("There are no more spaces for that specific position.");
            return false;
        }
        if (player.getRefToCurrentTeam().sumArray() >= 0 && player.getContract() == Contract.X) {
            alert.show("You must fill up your Standing Line Up before adding to the Draft. Please chose a different Contract");
            return false;
        }
        return true;
    }

    public void loadPlayer(Player player2) {
        positionsLabel.setText(player2.getPositionsList().toString());
        nameLabel.setText(player2.getFirstName() + " " + player2.getLastName());
        image = getImage("players", player2.getLastName() + player2.getFirstName(), ".jpg");
        playerImageView.setImage(image);
        flagImage = getImage("flags", player2.getNob(), ".png");
        flagImageView.setImage(flagImage);
        positionComboBox.setItems(player2.getPositionsList());
        salaryTextField.setText(String.valueOf(player2.getSalary()));
        contractComboBox.getSelectionModel().select(player2.getContract());
        positionComboBox.getSelectionModel().select(player2.getCurrentPos());
        getFantasyTeamComboBox().setItems(draft.getFantasyTeams());
        getFantasyTeamComboBox().getSelectionModel().select(player2.getRefToCurrentTeam());
    }

    public void setPlayerOne(Player player2) {
        player.setLastName(player2.getLastName());
        player.setFirstName(player2.getFirstName());
        player.setPositionsList(player2.getPositionsList());
        player.setHrSV(player2.getHrSV());
        player.setBaWhip(player2.getBaWhip());
        player.setRW(player2.getRW());
        player.setContract(player2.getContract());
        player.setRbiK(player2.getRbiK());
        player.setSbEra(player2.getSbEra());
        player.setIsPitcher(player2.isIsPitcher());
        player.setNob(player2.getNob());
        player.setContract(player2.getContract());
        player.setBirthYear(player2.getBirthYear());
        player.setNotes(player2.getNotes());
        player.setSalary(player2.getSalary());
        player.setFanTeamListPos(player2.getFanTeamListPos());
        player.setCurrentPos(player2.getCurrentPos());
        player.setRefToCurrentTeam(player2.getRefToCurrentTeam());

    }

    public void resetFields() {
        firstNameField.setText(player.getFirstName());
        lastNameField.setText(player.getLastName());
        proTeamComboBox.getSelectionModel().clearSelection();
        for (int i = 0; i < positionPane.getChildren().size(); i++) {
            CheckBox currentCheckBox = (CheckBox) positionPane.getChildren().get(i);
            currentCheckBox.setSelected(false);
        }
    }

    public boolean wasCompletedSelected() {
        return getSelection().equals(COMPLETE);
    }

    public Player getPlayer() {
        return player;
    }

    public static Image getImage(String folder, String name, String fileext) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + folder + "/" + name + fileext;
        Image image = new Image(imagePath);
        if (image.isError()) {
            return new Image("file:" + PATH_IMAGES + "players" + "/" + "AAA_PhotoMissing" + ".jpg");
        }
        return image;
    }

    /**
     * @return the contractComboBox
     */
    public ComboBox getContractComboBox() {
        return contractComboBox;
    }

    /**
     * @param contractComboBox the contractComboBox to set
     */
    public void setContractComboBox(ComboBox contractComboBox) {
        this.contractComboBox = contractComboBox;
    }

    /**
     * @return the salaryTextField
     */
    public TextField getSalaryTextField() {
        return salaryTextField;
    }

    /**
     * @param salaryTextField the salaryTextField to set
     */
    public void setSalaryTextField(TextField salaryTextField) {
        this.salaryTextField = salaryTextField;
    }

    /**
     * @return the destinationList
     */
    public ObservableList<Player> getDestinationList() {
        return destinationList;
    }

    /**
     * @param destinationList the destinationList to set
     */
    public void setDestinationList(ObservableList<Player> destinationList) {
        this.destinationList = destinationList;
    }

    /**
     * @return the fantasyTeamComboBox
     */
    public ComboBox getFantasyTeamComboBox() {
        return fantasyTeamComboBox;
    }

    /**
     * @param fantasyTeamComboBox the fantasyTeamComboBox to set
     */
    public void setFantasyTeamComboBox(ComboBox fantasyTeamComboBox) {
        this.fantasyTeamComboBox = fantasyTeamComboBox;
    }

    /**
     * @return the selection
     */
    public String getSelection() {
        return selection;
    }

    /**
     * @param selection the selection to set
     */
    public void setSelection(String selection) {
        this.selection = selection;
    }

}
