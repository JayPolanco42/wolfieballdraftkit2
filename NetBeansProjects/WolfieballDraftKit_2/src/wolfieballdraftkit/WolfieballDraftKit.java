/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieballdraftkit;

import java.util.Locale;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.property.IntegerProperty;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.file.JSONFileManager;
import wdk.gui.WDK_GUI;
import wdkdata.DraftData;
import static wolfieballdraftkit.WDK_Constants.PATH_DATA;
import static wolfieballdraftkit.WDK_Constants.PROPERTIES_FILE_NAME;
import static wolfieballdraftkit.WDK_Constants.PROPERTIES_SCHEMA_FILE_NAME;
import xml_utilities.InvalidXMLFileFormatException;

/**
 *
 * @author Jay
 */
public class WolfieballDraftKit extends Application {

    WDK_GUI gui;
    JSONFileManager jsonManager;

    public void start(Stage primaryStage) {
        boolean success = loadProperties();
        gui = new WDK_GUI(primaryStage);
        jsonManager = new JSONFileManager();
        gui.setDraftFileManager(jsonManager);
        gui.initGUI();
//        gui.initEventHandlers();
        gui.getStage().show();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);
        launch(args);
    }

    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
        } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
//            ErrorHandler eH = ErrorHandler.getErrorHandler();
//            eH.handlePropertiesFileError();
//            return false;
        }
        return false;
    }

}
