/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdkdata;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wdkdata.DraftData.*;

/**
 *
 * @author Jay
 */
public class Player {

    private StringProperty firstName;
    private StringProperty lastName;
    private ObjectProperty<MLBTeams> proTeam;
    private IntegerProperty birthYear;
    private ObservableList<String> positionsList;
    private IntegerProperty RW;
    private IntegerProperty hrSV;
    private IntegerProperty rbiK;
    private DoubleProperty sbEra;
    private DoubleProperty baWhip;
    private IntegerProperty salary;
    private StringProperty nob;
    private StringProperty notes;
    private StringProperty currentPos;
    private ObjectProperty contract;
    private String fanTeamListPos;
    private boolean isPitcher;
    private Team refToCurrentTeam;
    private IntegerProperty estimatedValue;

    private static String DEFAULT_NAME = "<ENTER NAME>";
    private static String DEFAULT_TEAM = "<ENTER TEAM>";
    private static int DEFAULT_SALARY = 0;

    public Player(String firstName, String lastName, String proTeam, int birthYear, int RW, int rbiK, int hrSV, double sbEra, double baWhip,
            int salary, String notes, boolean isPitcher, String nob
    ) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.proTeam = new SimpleObjectProperty(MLBTeams.valueOf(proTeam));//TODO
        this.birthYear = new SimpleIntegerProperty(birthYear);
        positionsList = FXCollections.observableArrayList();//TODO
        this.RW = new SimpleIntegerProperty(RW);
        this.rbiK = new SimpleIntegerProperty(rbiK);
        this.hrSV = new SimpleIntegerProperty(hrSV);
        this.sbEra = new SimpleDoubleProperty(sbEra);
        this.baWhip = new SimpleDoubleProperty(baWhip);
        this.salary = new SimpleIntegerProperty(salary);
        this.notes = new SimpleStringProperty(notes);
        this.nob = new SimpleStringProperty(nob);
        this.contract = new SimpleObjectProperty(Contract.valueOf("X"));
        this.isPitcher = isPitcher;
        this.currentPos = new SimpleStringProperty("");
        this.refToCurrentTeam = null;
        estimatedValue = new SimpleIntegerProperty(0);
    }

    public Player() {
        this.firstName = new SimpleStringProperty(DEFAULT_NAME);
        this.lastName = new SimpleStringProperty(DEFAULT_NAME);
        this.proTeam = new SimpleObjectProperty(MLBTeams.ATL);//TODO
        this.birthYear = new SimpleIntegerProperty(0);
        positionsList = FXCollections.observableArrayList();//TODO
        this.RW = new SimpleIntegerProperty(0);
        this.rbiK = new SimpleIntegerProperty(0);
        this.hrSV = new SimpleIntegerProperty(0);
        this.sbEra = new SimpleDoubleProperty(0.0);
        this.baWhip = new SimpleDoubleProperty(0.0);
        this.salary = new SimpleIntegerProperty(0);
        this.notes = new SimpleStringProperty("Notes");
        this.currentPos = new SimpleStringProperty("");
        this.contract = new SimpleObjectProperty(Contract.valueOf("X"));
        this.nob = new SimpleStringProperty("USA");
        this.isPitcher = true;
        this.refToCurrentTeam = null;
        estimatedValue = new SimpleIntegerProperty(0);
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName.get();
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = new SimpleStringProperty(firstName);
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName.get();
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = new SimpleStringProperty(lastName);
    }

    /**
     * @return the proTeam
     */
    public MLBTeams getProTeam() {
        return proTeam.getValue();
    }

    /**
     * @param proTeam the proTeam to set
     */
    public void setProTeam(String proTeam) {
        this.proTeam = new SimpleObjectProperty(MLBTeams.valueOf(proTeam));
    }

    /**
     * @return the birthYear
     */
    public int getBirthYear() {
        return birthYear.get();
    }

    /**
     * @param birthYear the birthYear to set
     */
    public void setBirthYear(int birthYear) {
        this.birthYear = new SimpleIntegerProperty(birthYear);
    }

    /**
     * @return the rW
     */
    public int getRW() {
        return RW.get();
    }

    /**
     * @param rW the rW to set
     */
    public void setRW(int RW) {
        this.RW = new SimpleIntegerProperty(RW);
    }

    /**
     * @return the hrSV
     */
    public int getHrSV() {
        return hrSV.get();
    }

    /**
     * @param hrSV the hrSV to set
     */
    public void setHrSV(int hrSV) {
        this.hrSV = new SimpleIntegerProperty(hrSV);
    }

    /**
     * @return the salary
     */
    public int getSalary() {
        return salary.get();
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(int salary) {
        this.salary = new SimpleIntegerProperty(salary);
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes.get();
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = new SimpleStringProperty(notes);
    }

    /**
     * @return the positionsList
     */
    public ObservableList<String> getPositionsList() {
        return positionsList;
    }

    /**
     * @param positionsList the positionsList to set
     */
    public void setPositionsList(List<String> positionsList) {

        ObservableList<String> checkList = FXCollections.observableArrayList();
        checkList.addAll(positionsList);
        if (checkList.contains("1B") || checkList.contains("3B")) {
             if(!checkList.contains("MI")){
            checkList.add("MI");
            }
        }
        if ((checkList.contains("2B") || checkList.contains("SS"))) {
            if(!checkList.contains("CI")){
            checkList.add("CI");
            }
        }
        if (!checkList.contains("U")&&(checkList.contains("1B") || checkList.contains("2B") || checkList.contains("3B") || checkList.contains("C") || checkList.contains("SS") || checkList.contains("OF")||checkList.contains("MI")||checkList.contains("CI"))) {
            checkList.add("U");
        }
        this.getPositionsList().addAll(checkList);
    }
    public void addToPosList(String qualPos) {
        positionsList.add(qualPos);
    }

    /**
     * @return the rbiK
     */
    public int getRbiK() {
        return rbiK.get();
    }

    /**
     * @param rbiK the rbiK to set
     */
    public void setRbiK(int rbiK) {
        this.rbiK = new SimpleIntegerProperty(rbiK);
    }

    /**
     * @return the sbEra
     */
    public Number getSbEra() {
        DecimalFormat df = new DecimalFormat("#.00");
        if (!isPitcher) {
            return (int) sbEra.get();
        }
        if(Double.isNaN(sbEra.get())){
            return 0.0;
        }
       return Double.valueOf(df.format(sbEra.get()));
    }

    /**
     * @param sbEra the sbEra to set
     */
    public void setSbEra(Number sbEra) {
        this.sbEra = new SimpleDoubleProperty(sbEra.doubleValue());
    }

    /**
     * @return the baWhip
     */
    public double getBaWhip() {
        DecimalFormat df = new DecimalFormat("#.00");
        DecimalFormat df2 = new DecimalFormat("#.000");
        if (Double.isNaN(baWhip.get())) {
            return Double.valueOf(df2.format(0));
        }
        if(isIsPitcher()){
            return Double.valueOf(df2.format(baWhip.get()));
        }
        else{
            return Double.valueOf(df.format(baWhip.get()));

        }
        //TODO LEADING 0
    }

    /**
     * @param baWhip the baWhip to set
     */
    public void setBaWhip(double baWhip) {

        this.baWhip = new SimpleDoubleProperty(baWhip);
    }

    /**
     * @return the isPitcher
     */
    public boolean isIsPitcher() {
        return isPitcher;
    }

    /**
     * @param isPitcher the isPitcher to set
     */
    public void setIsPitcher(boolean isPitcher) {
        this.isPitcher = isPitcher;
    }

    /**
     * @return the nob
     */
    public String getNob() {
        return nob.getValue();
    }

    /**
     * @param nob the nob to set
     */
    public void setNob(String nob) {
        this.nob = new SimpleStringProperty(nob);
    }

    /**
     * @return the currentPos
     */
    public String getCurrentPos() {
        return currentPos.getValue();
    }

    /**
     * @param currentPos the currentPos to set
     */
    public void setCurrentPos(String currentPos) {
        this.currentPos = new SimpleStringProperty(currentPos);
    }

    /**
     * @return the contract
     */
    /**
     * @return the fanTeamListPos
     */
    public String getFanTeamListPos() {
        return fanTeamListPos;
    }

    /**
     * @param fanTeamListPos the fanTeamListPos to set
     */
    public void setFanTeamListPos(String fanTeamListPos) {
        this.fanTeamListPos = fanTeamListPos;
    }

    /**
     * @return the contract
     */
    public Contract getContract() {
        return (Contract) contract.getValue();
    }

    /**
     * @param contract the contract to set
     */
    public void setContract(Contract contract) {
        this.contract = new SimpleObjectProperty(contract);
    }

    /**
     * @return the refToCurrentTeam
     */
    public Team getRefToCurrentTeam() {
        return refToCurrentTeam;
    }

    /**
     * @param refToCurrentTeam the refToCurrentTeam to set
     */
    public void setRefToCurrentTeam(Team refToCurrentTeam) {
        this.refToCurrentTeam = refToCurrentTeam;
    }

    /**
     * @return the estimatedValue
     */
    public int getEstimatedValue() {
        return estimatedValue.get();
    }

    /**
     * @param estimatedValue the estimatedValue to set
     */
    public void setEstimatedValue(int estimatedValue) {
        this.estimatedValue = new SimpleIntegerProperty(estimatedValue);
    }
    
    
    
}
