/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdkdata;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Jay
 */
    
public class DraftData {
    // INIT THE DRAFT STUFF
    
    private ObservableList<Team> fantasyTeams;
    private Team freeAgents;
    private String titleOfDraft;
    private ObservableList<Player> draftedPlayers;
    
    public DraftData() {
        fantasyTeams = FXCollections.observableArrayList();
        draftedPlayers = FXCollections.observableArrayList();
    }

    /**
     * @return the fantasyTeams
     */
    

    /**
     * @param fantasyTeams the fantasyTeams to set
     */
   

    /**
     * @return the fantasyTeams
     */
    public ObservableList<Team> getFantasyTeams() {
        return fantasyTeams;
    }
   
    
    public void addTeam(Team team){
        fantasyTeams.add(team);
    }

    /**
     * @param fantasyTeams the fantasyTeams to set
     */
    public void setFantasyTeams(ObservableList<Team> fantasyTeams) {
        this.fantasyTeams = fantasyTeams;
    }

    /**
     * @return the titleOfDraft
     */
    public String getTitleOfDraft() {
        return titleOfDraft;
    }

    /**
     * @param titleOfDraft the titleOfDraft to set
     */
    public void setTitleOfDraft(String titleOfDraft) {
        this.titleOfDraft = titleOfDraft;
    }

    /**
     * @return the freeAgents
     */
    public Team getFreeAgents() {
        return freeAgents;
    }

    /**
     * @param freeAgents the freeAgents to set
     */
    public void setFreeAgents(Team freeAgents) {
        this.freeAgents = freeAgents;
    }

    /**
     * @return the draftedPlayers
     */
    public ObservableList<Player> getDraftedPlayers() {
        return draftedPlayers;
    }

    /**
     * @param draftedPlayers the draftedPlayers to set
     */
    public void setDraftedPlayers(ObservableList<Player> draftedPlayers) {
        this.draftedPlayers = draftedPlayers;
    }
    
    public void updateEstimatedValues(){
        int sumOfMoney=0;
        
        for(Team team: this.fantasyTeams){
            sumOfMoney+=team.getMoneyLeft();
        }
        for(Player player: this.getFreeAgents().getPlayerList()){
            player.setEstimatedValue((int) (sumOfMoney/((player.getRW()+player.getBaWhip()+player.getHrSV()+player.getRbiK()+Double.valueOf(player.getSbEra().toString())))));
        }
        
        
    }
    
}