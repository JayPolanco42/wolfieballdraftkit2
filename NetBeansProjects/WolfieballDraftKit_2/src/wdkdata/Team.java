/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdkdata;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.w3c.dom.css.Counter;

/**
 *
 * @author Jay
 */
public class Team {
    
    private StringProperty teamName;
    private StringProperty ownerName;
    private ObservableList<Player> playerList;
    private IntegerProperty playerSpaces;
    private IntegerProperty moneyLeft;
    private IntegerProperty moneyPerPlayer;
    private IntegerProperty rSum;
    private IntegerProperty hrSum;
    private IntegerProperty rbiSum;
    private IntegerProperty sbSum;
    private DoubleProperty baAvg;
    private IntegerProperty wSum;
    private IntegerProperty svSum;
    private IntegerProperty kSum;
    private DoubleProperty eraAvg;
    private DoubleProperty whipAvg;
    private IntegerProperty totalPoints;
    
    private int[] posAvail;
    
    private static String DEFAULT_NAME = "<ENTER NAME>";
    private static String DEFAULT_OWNER = "<ENTER OWNER>";
    
    public Team(String teamName, String ownerName) {
        posAvail = new int[10];
        resetPosAvail();
        this.teamName = new SimpleStringProperty(teamName);
        this.ownerName = new SimpleStringProperty(ownerName);
        playerList = FXCollections.observableArrayList();
        playerSpaces = new SimpleIntegerProperty(sumArray());
        moneyLeft = new SimpleIntegerProperty(260);
        moneyPerPlayer = new SimpleIntegerProperty(0);
        rSum = new SimpleIntegerProperty(0);
        sbSum = new SimpleIntegerProperty(0);
        hrSum = new SimpleIntegerProperty(0);
        rbiSum = new SimpleIntegerProperty(0);
        baAvg = new SimpleDoubleProperty(0.0);
        wSum = new SimpleIntegerProperty(0);
        svSum = new SimpleIntegerProperty(0);
        kSum = new SimpleIntegerProperty(0);
        eraAvg = new SimpleDoubleProperty(0.0);
        whipAvg = new SimpleDoubleProperty(0.0);
        totalPoints = new SimpleIntegerProperty(0);
        
    }
    
    public int sumArray() {
        int sum = 0;
        for (int x : posAvail) {
            sum += x;
        }
        return sum;
    }
    
    public Team() {
        posAvail = new int[10];
        resetPosAvail();
        this.ownerName = new SimpleStringProperty(DEFAULT_OWNER);
        this.teamName = new SimpleStringProperty(DEFAULT_NAME);
        playerList = FXCollections.observableArrayList();
        playerSpaces = new SimpleIntegerProperty(sumArray());
        moneyLeft = new SimpleIntegerProperty(260);
        moneyPerPlayer = new SimpleIntegerProperty(0);
        rSum = new SimpleIntegerProperty(0);
        sbSum = new SimpleIntegerProperty(0);
        hrSum = new SimpleIntegerProperty(0);
        rbiSum = new SimpleIntegerProperty(0);
        baAvg = new SimpleDoubleProperty(0.0);
        wSum = new SimpleIntegerProperty(0);
        svSum = new SimpleIntegerProperty(0);
        kSum = new SimpleIntegerProperty(0);
        eraAvg = new SimpleDoubleProperty(0.0);
        whipAvg = new SimpleDoubleProperty(0.0);
        totalPoints = new SimpleIntegerProperty(0);
        
    }
    
    public void updateTeamStats() {
        int moneyLeftUpdate = 260;
        int runsUpdate = 0;
        int sbUpdate = 0;
        int rbiUpdate = 0;
        int wUpdate = 0;
        int rUpdate = 0;
        int svUpdate = 0;
        int hrUpdate = 0;
        double eraAvg = 0.0;
        double whipAvg = 0.0;
        double baAvg = 0.0;
        int numOfPitchers = 0;
        int numOfHitters = 0;
        int kSum = 0;
        int rounder;
        for (Player player : this.getPlayerList()) {
            moneyLeftUpdate -= player.getSalary();
            if (!player.isIsPitcher()) {
                runsUpdate += player.getRW();
                sbUpdate += (int) player.getSbEra();
                rbiUpdate += player.getRbiK();
                rUpdate += player.getRW();
                hrUpdate += player.getHrSV();
                baAvg += (double) player.getBaWhip();
                numOfHitters++;
            } else if (player.isIsPitcher()) {
                wUpdate += player.getRW();
                svUpdate += player.getHrSV();
                eraAvg += (double) player.getSbEra();
                whipAvg += (double) player.getBaWhip();
                kSum += player.getRbiK();
                numOfPitchers++;
            }
        }
        int sumArray = sumArray();
        this.setPlayerSpaces(sumArray);
        this.setMoneyLeft(moneyLeftUpdate);
        if (sumArray != 0) {
            this.setMoneyPerPlayer(((moneyLeftUpdate / sumArray) + 1));
            
        } else {
            this.setMoneyPerPlayer(0);
        }
        this.setRSum(runsUpdate);
        this.setSbSum(sbUpdate);
        this.setRbiSum(rbiUpdate);
        this.setRSum(rUpdate);
        this.setHrSum(hrUpdate);
        this.setBaAvg(baAvg / numOfHitters);
        this.setEraAvg(eraAvg / numOfPitchers);
        this.setWhipAvg(whipAvg / numOfPitchers);
        this.setWSum(wUpdate);
        this.setSvSum(svUpdate);
        this.setKSum(kSum);
    }

    /**
     * @return the teamName
     */
    public void resetPosAvail() {
        posAvail[0] = 2;
        posAvail[1] = 1;
        posAvail[2] = 1;
        posAvail[3] = 1;
        posAvail[4] = 1;
        posAvail[5] = 1;
        posAvail[6] = 1;
        posAvail[7] = 5;
        posAvail[8] = 1;
        posAvail[9] = 9;
    }
    
    public int countTaxi() {
        int counter = 0;
        for (Player player : this.getPlayerList()) {
            if (player.getCurrentPos().equals("Taxi")) {
                counter++;
            }
        }
        return counter;
    }
    
    public void countTeam() {
        int position = 0;
        resetPosAvail();
        for (Player player : this.getPlayerList()) {
            if (player.getCurrentPos().equals("C")) {
                position = 0;
            } else if (player.getCurrentPos().equals("1B")) {
                position = 1;
            } else if (player.getCurrentPos().equals("3B")) {
                position = 2;
            } else if (player.getCurrentPos().equals("CI")) {
                position = 3;
            } else if (player.getCurrentPos().equals("2B")) {
                position = 4;
            } else if (player.getCurrentPos().equals("SS")) {
                position = 5;
            } else if (player.getCurrentPos().equals("MI")) {
                position = 6;
            } else if (player.getCurrentPos().equals("OF")) {
                position = 7;
            } else if (player.getCurrentPos().equals("U")) {
                position = 8;
            } else if (player.getCurrentPos().equals("P")) {
                position = 9;
            }
            posAvail[position]--;
        }
        this.setPlayerSpaces(sumArray());
    }

    public int positionAvailable() {
        for (int i = 0; i < this.posAvail.length; i++) {
            if (posAvail[i] != 0) {
                return i;
            }
        }
        return -1;
    }
    
    public boolean spaceLeft(Player player) {
        int position = 0;
        if (player.getCurrentPos().equals("C")) {
            position = 0;
        } else if (player.getCurrentPos().equals("1B")) {
            position = 1;
        } else if (player.getCurrentPos().equals("3B")) {
            position = 2;
        } else if (player.getCurrentPos().equals("CI")) {
            position = 3;
        } else if (player.getCurrentPos().equals("2B")) {
            position = 4;
        } else if (player.getCurrentPos().equals("SS")) {
            position = 5;
        } else if (player.getCurrentPos().equals("MI")) {
            position = 6;
        } else if (player.getCurrentPos().equals("OF")) {
            position = 7;
        } else if (player.getCurrentPos().equals("U")) {
            position = 8;
        } else if (player.getCurrentPos().equals("P")) {
            position = 9;
        }
        if (posAvail[position] <= 0) {
            return false;
        }
        return true;
    }
    
    public String getTeamName() {
        return teamName.get();
    }

    /**
     * @param teamName the teamName to set
     */
    public void setTeamName(String teamName) {
        this.teamName = new SimpleStringProperty(teamName);
    }

    /**
     * @return the ownerName
     */
    public String getOwnerName() {
        return ownerName.get();
    }

    /**
     * @param ownerName the ownerName to set
     */
    public void setOwnerName(String ownerName) {
        this.ownerName = new SimpleStringProperty(ownerName);
    }

    /**
     * @return the playerList
     */
    public ObservableList<Player> getPlayerList() {
        return playerList;
    }

    /**
     * @param playerList the playerList to set
     */
    public void setPlayerList(ObservableList<Player> playerList) {
        this.playerList = playerList;
    }

    /**
     * @return the posAvail
     */
    @Override
    public String toString() {
        return this.getTeamName();
    }

    /**
     * @return the posAvail
     */
    public int[] getPosAvail() {
        return posAvail;
    }

    /**
     * @param posAvail the posAvail to set
     */
    public void setPosAvail(int[] posAvail) {
        this.posAvail = posAvail;
    }

    /**
     * @return the playerSpaces
     */
    public int getPlayerSpaces() {
        return playerSpaces.get();
    }

    /**
     * @param playerSpaces the playerSpaces to set
     */
    public void setPlayerSpaces(int playerSpaces) {
        this.playerSpaces = new SimpleIntegerProperty(playerSpaces);
    }

    /**
     * @return the moneyLeft
     */
    public int getMoneyLeft() {
        return moneyLeft.get();
    }

    /**
     * @param moneyLeft the moneyLeft to set
     */
    public void setMoneyLeft(int moneyLeft) {
        this.moneyLeft = new SimpleIntegerProperty(moneyLeft);
    }

    /**
     * @return the moneyPerPlayer
     */
    public int getMoneyPerPlayer() {
        return moneyPerPlayer.get();
    }

    /**
     * @param moneyPerPlayer the moneyPerPlayer to set
     */
    public void setMoneyPerPlayer(int moneyPerPlayer) {
        this.moneyPerPlayer = new SimpleIntegerProperty(moneyPerPlayer);
    }

    /**
     * @return the rSum
     */
    public int getRSum() {
        return rSum.get();
    }

    /**
     * @param rSum the rSum to set
     */
    public void setRSum(int rSum) {
        this.rSum = new SimpleIntegerProperty(rSum);
    }

    /**
     * @return the hrSum
     */
    public int getHrSum() {
        return hrSum.get();
    }

    /**
     * @param hrSum the hrSum to set
     */
    public void setHrSum(int hrSum) {
        this.hrSum = new SimpleIntegerProperty(hrSum);
    }

    /**
     * @return the rbiSum
     */
    public int getRbiSum() {
        return rbiSum.get();
    }

    /**
     * @param rbiSum the rbiSum to set
     */
    public void setRbiSum(int rbiSum) {
        this.rbiSum = new SimpleIntegerProperty(rbiSum);
    }

    /**
     * @return the sbSum
     */
    public int getSbSum() {
        return sbSum.get();
    }

    /**
     * @param sbSum the sbSum to set
     */
    public void setSbSum(int sbSum) {
        this.sbSum = new SimpleIntegerProperty(sbSum);
    }

    /**
     * @return the baAvg
     */
    public double getBaAvg() {
        DecimalFormat df2 = new DecimalFormat("#.000");
        if (Double.isNaN(baAvg.get())) {
            return 0.0;
        }
        return Double.valueOf(df2.format(baAvg.get()));
    }

    /**
     * @param baAvg the baAvg to set
     */
    public void setBaAvg(double baAvg) {
        this.baAvg = new SimpleDoubleProperty(baAvg);
    }

    /**
     * @return the wSum
     */
    public int getWSum() {
        return wSum.get();
    }

    /**
     * @param wSum the wSum to set
     */
    public void setWSum(int wSum) {
        this.wSum = new SimpleIntegerProperty(wSum);
    }

    /**
     * @return the svSum
     */
    public int getSvSum() {
        return svSum.get();
    }

    /**
     * @param svSum the svSum to set
     */
    public void setSvSum(int svSum) {
        this.svSum = new SimpleIntegerProperty(svSum);
    }

    /**
     * @return the kSum
     */
    public int getKSum() {
        return kSum.get();
    }

    /**
     * @param kSum the kSum to set
     */
    public void setKSum(int kSum) {
        this.kSum = new SimpleIntegerProperty(kSum);
    }

    /**
     * @return the eraAvg
     */
    public double getEraAvg() {
        DecimalFormat df = new DecimalFormat("#.00");
        if (Double.isNaN(eraAvg.get())) {
            return 0.0;
        }
        return Double.valueOf(df.format(this.eraAvg.get()));
    }

    /**
     * @param eraAvg the eraAvg to set
     */
    public void setEraAvg(double eraAvg) {
        this.eraAvg = new SimpleDoubleProperty(eraAvg);
    }

    /**
     * @return the whipAvg
     */
    public double getWhipAvg() {
        DecimalFormat df = new DecimalFormat("#.00");
        if (Double.isNaN(whipAvg.get())) {
            return 0.0;
        }
        return Double.valueOf(df.format(whipAvg.get()));
    }

    /**
     * @param whipAvg the whipAvg to set
     */
    public void setWhipAvg(double whipAvg) {
        this.whipAvg = new SimpleDoubleProperty(whipAvg);
    }

    /**
     * @return the totalPoints
     */
    public int getTotalPoints() {
        return totalPoints.get();
    }

    /**
     * @param totalPoints the totalPoints to set
     */
    public void setTotalPoints(int totalPoints) {
        this.totalPoints = new SimpleIntegerProperty(totalPoints);
    }
    
}
